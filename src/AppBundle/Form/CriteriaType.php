<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CriteriaType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null, array(
                'label' => 'Value',
            ))
            ->add('alternateNames', null, array(
                'label' => 'Altername names (Separated by comma)',
            ))
            ->add('positiveRemarks', null, array(
                'label' => 'Positive Remarks (Separated by comma)',
            ))
            ->add('negativeRemarks', null, array(
                'label' => 'Negative Remarks (Separated by comma)',
            ))
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Criteria',
        ));
    }
}
