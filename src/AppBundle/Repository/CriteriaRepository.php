<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Criteria;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;

class CriteriaRepository extends EntityRepository
{
    /**
     * @return Query
     */
    public function queryLatest()
    {
        return $this->getEntityManager()
            ->createQuery('
                SELECT p
                FROM AppBundle:Criteria p
                WHERE p.insertedAt <= :now
                ORDER BY p.insertedAt DESC
            ')
            ->setParameter('now', new \DateTime())
        ;
    }

    /**
     * @param String $criteriaName
     *
     * @return Criteria criteria
     */
    public function getCriteriaByName($criteriaName)
    {
        $query = $this->getEntityManager()
            ->createQuery('
                SELECT p as criteria
                FROM AppBundle:Criteria p
                WHERE p.name = :name
                ORDER BY p.insertedAt DESC
            ')
            ->setParameter('name', $criteriaName)
        ;

        if (sizeof($query->getResult()) == 0) {
          return null;
        } else {
          return $query->getResult()[0]['criteria'];
        }
    }

    /**
     * @param int $page
     *
     * @return Pagerfanta
     */
    public function findLatest($page = 1)
    {
        $paginator = new Pagerfanta(new DoctrineORMAdapter($this->queryLatest(), false));
        $paginator->setMaxPerPage(Criteria::NUM_ITEMS);
        $paginator->setCurrentPage($page);

        return $paginator;
    }

    public function getAllCriteria()
    {
        $paginator = new Pagerfanta(new DoctrineORMAdapter($this->queryLatest(), false));
        $paginator->setMaxPerPage(Criteria::NUM_ITEMS);
        $paginator->setCurrentPage(1);

        return $paginator;
    }
}
