<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Review;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Pagerfanta\Adapter\DoctrineORMAdapter;
use Pagerfanta\Pagerfanta;

class ReviewRepository extends EntityRepository
{
    /**
     * @return Query
     */
    public function queryLatest()
    {
        return $this->getEntityManager()
            ->createQuery('
                SELECT p
                FROM AppBundle:Review p
                WHERE p.insertedAt <= :now
                ORDER BY p.insertedAt DESC
            ')
            ->setParameter('now', new \DateTime())
        ;
    }

    /**
     * @param int $stockId
     *
     * @return int stockId
     */
    public function nextReviewId($stockId)
    {
        $query = $this->getEntityManager()
            ->createQuery('
                SELECT COALESCE(max(p.reviewId) + 1, 1) as reviewId
                FROM AppBundle:Review p
                WHERE p.stockId = :stockId
                ORDER BY p.insertedAt DESC
            ')
            ->setParameter('stockId', $stockId)
        ;

        return intval($query->getResult()[0]['reviewId']);
    }

    /**
     * @param int $page
     *
     * @return Pagerfanta
     */
    public function findLatest($page = 1)
    {
        $paginator = new Pagerfanta(new DoctrineORMAdapter($this->queryLatest(), false));
        $paginator->setMaxPerPage(Review::NUM_ITEMS);
        $paginator->setCurrentPage($page);

        return $paginator;
    }
}
