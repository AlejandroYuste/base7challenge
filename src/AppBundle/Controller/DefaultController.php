<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Review;
use AppBundle\Entity\Criteria;
use AppBundle\Loaders\CVSLoader;

class DefaultController extends Controller
{
    /**
     * @Route("/", defaults={"page": 1}, name="homepage")
     * @Route("/page/{page}", requirements={"page": "[1-9]\d*"}, name="homepage_paginated")
     */
     public function showReviews($page)
    {
        $reviews = $this->getDoctrine()->getRepository('AppBundle:Review')->findLatest($page);
        return $this->render('reviews/review.html.twig', array('reviews' => $reviews));
    }

    /**
     * @Route("/criteria", defaults={"page": 1}, name="criteria")
     * @Route("/page/{page}", requirements={"page": "[1-9]\d*"}, name="criteria_paginated")
     */
    public function showCrteiria($page)
    {
        $criteria = $this->getDoctrine()->getRepository('AppBundle:Criteria')->findLatest($page);
        return $this->render('criteria/criteria.html.twig', array('criteria' => $criteria));
    }

    /**
     * @Route("/", defaults={"page": 1}, name="evaluate_all")
     */
    public function evalAll($page)
    {
        $reviews = $this->getDoctrine()->getRepository('AppBundle:Review')->findLatest($page);

        foreach ($review as $rev) {
            $this->evalReview($review);
        }

        return $this->redirectToRoute('homepage');
    }

    /**
     * @Route("/{id}/eval", requirements={"id": "\d+"}, name="eval_review")
     */
    public function evalReviewPage(Review $review)
    {
        $this->evalReview($review);
        return $this->redirectToRoute('homepage');
    }

    private function evalReview(Review $review) {
        $reviewText = $review->getReviewText();
        //Text with only natural words.
        $quote = '"';
        $textToEvalTemp = strtolower(ereg_replace("(\.)|(\,)|(\:)|(\;)|(\-)|(\?)(\!)|(\()|(\))|(\$)", " ", ereg_replace("'", "", $reviewText)));
        //Remove useless words. We could add much as we want, should not affect review and put attributes closer to nouns.
        $textToEval = ereg_replace("(than)|(really)|(quite)|(the)|(with)|(very)(more)|(so)|(this))|(\$)", "", $textToEvalTemp);
        $newScore = '';
        $newTotal = 0;

        $criteria = $this->getDoctrine()->getRepository('AppBundle:Criteria')->getAllCriteria();
        foreach ($criteria as $crit) {
            foreach(explode(',', $crit->getAlternateNames()) as $altName) {
                //RegExp to find the alternate names in the text.
                if (strlen($altName) > 1 && preg_match("/.{0,20}(" . $altName . ")(s)?.{0,20}/", $textToEval, $matchedText)) {
                    /**
                     * If the word is found, We'll check the words around
                     * the match for positive and negative remarks.
                     * Let's says 20 characters backwards and forwards.
                     */
                     foreach($matchedText as $regExpHit) {
                        foreach(explode(',', $crit->getPositiveRemarks()) as $posWord) {
                            //Taken negations into account in 10 characers before positive atribute.
                            if (strlen($posWord) > 1 && preg_match("/(?!not|dont){0,10}(".$posWord.")/", $regExpHit)) {
                                $newScore = $newScore . "\n" . $altName . "/". $posWord . ": +1,";
                                $newTotal++;
                            }
                        }
                        foreach(explode(',', $crit->getNegativeRemarks()) as $negWord) {
                            //Taken negations into account in 10 characers before negative atribute.
                            if (strlen($negWord) > 1 && preg_match("/(?!not|dont){0,10}(".$negWord.")/", $regExpHit)) {
                                $newScore = $newScore . "\n" . $altName . "/". $negWord . ": -1,";
                                $newTotal--;
                            }
                        }
                    }
                }
            }
        }

        $review->setScore($newScore);
        $review->setTotal($newTotal);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($review);
        $entityManager->flush();
    }

    /**
     * @Route("/upload_review_file", name="upload_review_file")
     */
    public function uploadReviewFile(Request $request)
    {
      $result = $this->get('CVSLoader')->uploadReviewFile($request, $this);

      if($result == 1) {
        return $this->redirectToRoute('homepage');
      } else {
        return $this->redirectToRoute('error_cvs');
      }

    }

    /**
     * @Route("/upload_criteria_file", name="upload_criteria_file")
     */
    public function uploadCriteriaFile(Request $request)
    {
      $result = $this->get('CVSLoader')->uploadCriteriaFile($request, $this);

      if($result == 1) {
        return $this->redirectToRoute('criteria');
      } else {
        return $this->redirectToRoute('error_cvs');
      }

    }

    /**
     * @Route("/error_cvs", defaults={"page": 1}, name="error_cvs")
     */
    public function erroCVS()
    {
        return $this->render('error_cvs.html.twig');
    }

    /**
     * @Route("/new_review", name="add_review")
     */
    public function newReview(Request $request)
    {
        $review = new Review();
        $form = $this->createForm('AppBundle\Form\ReviewType', $review)
            ->add('saveAndCreateNew', 'Symfony\Component\Form\Extension\Core\Type\SubmitType');

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->getErrors(true)) {
            $review->setReviewId($this->getDoctrine()->getRepository('AppBundle:Review')->nextReviewId($review->getStockId()));

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($review);
            $entityManager->flush();

            if ($form->get('saveAndCreateNew')->isClicked()) {
                return $this->redirectToRoute('add_review');
            }

            return $this->redirectToRoute('homepage');
        }

        return $this->render('reviews/add_review.html.twig', array(
            'review' => $review,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/{id}/edit_review", requirements={"id": "\d+"}, name="modify_review")
     */
     public function modifyReview(Request $request, Review $review)
    {
      $entityManager = $this->getDoctrine()->getManager();
      $editForm = $this->createForm('AppBundle\Form\ReviewType', $review);
      $deleteForm = $this->createDeleteForm($review);
      $editForm->handleRequest($request);

      if ($editForm->isSubmitted() && $editForm->getErrors(true)) {
          $entityManager = $this->getDoctrine()->getManager();
          $entityManager->persist($review);
          $entityManager->flush();

          return $this->redirectToRoute('homepage');
      }

      return $this->render('reviews/modify_review.html.twig', array(
          'review' => $review,
          'form' => $editForm->createView(),
      ));
    }

    /**
     * @Route("/{id}/remove_review", requirements={"id": "\d+"}, name="delete_review")
     */
     public function deleteReview(Request $request, Review $review)
    {
        $deleteForm = $this->createDeleteForm($review);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($review);
        $entityManager->flush();

        return $this->redirectToRoute('homepage');
    }

    /**
     * @Route("/new_criteria", name="add_criteria")
     */
    public function newCrtieria(Request $request)
    {
        $criteria = new Criteria();
        $form = $this->createForm('AppBundle\Form\CriteriaType', $criteria)
            ->add('saveAndCreateNew', 'Symfony\Component\Form\Extension\Core\Type\SubmitType');

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->getErrors(true)) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($criteria);
            $entityManager->flush();

            if ($form->get('saveAndCreateNew')->isClicked()) {
                return $this->redirectToRoute('add_criteria');
            }

            return $this->redirectToRoute('criteria');
        }

        return $this->render('criteria/add_criteria.html.twig', array(
            'criteria' => $criteria,
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/{id}/edit_criteria", requirements={"id": "\d+"}, name="modify_criteria")
     */
     public function modifyCriteria(Request $request, Criteria $criteria)
    {
      $entityManager = $this->getDoctrine()->getManager();
      $editForm = $this->createForm('AppBundle\Form\CriteriaType', $criteria);
      $deleteForm = $this->createDeleteFormCriteria($criteria);
      $editForm->handleRequest($request);

      if ($editForm->isSubmitted() && $editForm->getErrors(true)) {
          $entityManager = $this->getDoctrine()->getManager();
          $entityManager->persist($criteria);
          $entityManager->flush();

          return $this->redirectToRoute('criteria');
      }

      return $this->render('criteria/modify_criteria.html.twig', array(
          'criteria' => $criteria,
          'form' => $editForm->createView(),
      ));
    }

    /**
     * @Route("/{id}/remove_criteria", requirements={"id": "\d+"}, name="delete_criteria")
     */
     public function deleteCriteria(Request $request, Criteria $criteria)
    {
        $deleteForm = $this->createDeleteFormCriteria($criteria);

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($criteria);
        $entityManager->flush();

        return $this->redirectToRoute('criteria');
    }

    private function createDeleteForm(Review $review)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('homepage', array('id' => $review->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    private function createDeleteFormCriteria(Criteria $criteria)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('criteria', array('id' => $criteria->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
