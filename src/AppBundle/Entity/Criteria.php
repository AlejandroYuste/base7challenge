<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CriteriaRepository")
 * @ORM\Table(name="base7_crieria")
 */
class Criteria
{
    const NUM_ITEMS = 100;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @ORM\Column(type="datetime")
     * @Assert\DateTime()
     */
    private $insertedAt;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $alternateNames;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $positiveRemarks;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    private $negativeRemarks;


    public function __construct()
    {
        $this->insertedAt = new \DateTime();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getAlternateNames()
    {
        return $this->alternateNames;
    }

    public function setAlternateNames($alternateNames)
    {
        $this->alternateNames = $alternateNames;
    }

    public function getPositiveRemarks()
    {
        return $this->positiveRemarks;
    }

    public function setPositiveRemarks($postiveRemarks)
    {
        $this->positiveRemarks = $postiveRemarks;
    }

    public function getNegativeRemarks()
    {
        return $this->negativeRemarks;
    }

    public function SetNegativeRemarks($negativeRemarks)
    {
        $this->negativeRemarks = $negativeRemarks;
    }

    public function setInsertedAt(\DateTime $insertDate)
    {
        $this->insertedAt = $insertDate;
    }
}
