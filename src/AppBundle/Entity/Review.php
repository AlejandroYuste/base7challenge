<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ReviewRepository")
 * @ORM\Table(name="base7_review")
 */

class Review
{
    const NUM_ITEMS = 100;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $stockId;

    /**
     * @ORM\Column(type="integer")
     */
    private $reviewId;

    /**
     * @ORM\Column(type="string")
     */
    private $reviewText;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
     private $score;

    /**
    * @ORM\Column(type="string", nullable=true)
    */
    private $total;

    /**
     * @ORM\Column(type="datetime")
     * @Assert\DateTime()
     */
    private $insertedAt;

    /* Fields till here */
    public function __construct()
    {
        $this->insertedAt = new \DateTime();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getStockId()
    {
        return $this->stockId;
    }

    public function setStockId($stockId)
    {
        $this->stockId = $stockId;
    }

    public function getReviewId()
    {
        return $this->reviewId;
    }

    public function setReviewId($reviewId)
    {
        $this->reviewId = $reviewId;
    }

    public function getReviewText()
    {
        return $this->reviewText;
    }

    public function setReviewText($review)
    {
        $this->reviewText = $review;
    }

    public function getScore()
    {
        return $this->score;
    }

    public function setScore($score)
    {
        $this->score = $score;
    }

    public function getTotal()
    {
        return $this->total;
    }

    public function setTotal($total)
    {
        $this->total = $total;
    }

    public function getInsertedAt()
    {
        return $this->insertedAt;
    }

    public function setinsertedAt(\DateTime $insertedAt)
    {
        $this->insertedAt = $insertedAt;
    }
}
