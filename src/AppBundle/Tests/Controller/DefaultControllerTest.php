<?php

namespace AppBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class DefaultControllerTest extends WebTestCase
{
    /**
    * @dataProvider getPublicUrls
    */
    public function testPublicUrls($url)
    {
        $client = self::createClient();
        $client->request('GET', $url);

        $this->assertTrue(
            $client->getResponse()->isSuccessful(), sprintf('The %s public URL loads correctly.', $url)
        );
    }

    public function getPublicUrls()
    {
        return array(
            array('/'),
            array('/criteria'),
        );
    }
}
