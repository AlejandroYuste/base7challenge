<?php

namespace AppBundle\Loader;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\Review;
use AppBundle\Entity\Criteria;

class CVSLoader
{

    public function uploadReviewFile(Request $request, $controller)
    {
        /*I followed exactly the format of the CVS I found on the challenge.
        * HotelId;ReviewId;content;example score;total
        * I igonred the review id to avoid DB inconsitences.
        * I store the example score even if later on is reevaluated.
        */

        if ($request->getMethod('post') == 'POST') {
            $file = $request->files->get("reviewFile");

            if (is_null($file) || !($file->isValid())) {
                return 0;
            } else {
                if (($handle = fopen($file->getRealPath(), "r")) !== FALSE) {
                    $entityManager = $controller->getDoctrine()->getManager();

                    $i = 0;
                    while(($row = fgetcsv($handle)) !== FALSE) {
                        //Ignore first CVS row with titles.
                        if ($i == 0) {
                          $i++;
                          continue;
                        }

                        $review = new Review();
                        //Check if ids are numeric.
                        if(!is_numeric($row[0])) {
                          return 0;
                        }

                        $review->setStockId($row[0]);
                        $review->setReviewId($controller->getDoctrine()->getRepository('AppBundle:Review')->nextReviewId($row[0]));

                        //Check text is not empty.
                        if(strlen($row[2]) <= 0) {
                          return 0;
                        } else {
                          $review->setReviewText($row[2]);
                        }

                        //Next fields are nullabe, nothing is checked.
                        $review->setScore($row[3]);
                        $review->setTotal($row[4]);

                        $entityManager->persist($review);
                        $entityManager->flush();
                    }

                    return 1;
                }
            }
         }
    }

    public function uploadCriteriaFile(Request $request, $controller)
    {
        if ($request->getMethod('post') == 'POST') {
            $file = $request->files->get("criteriaFile");

            if (is_null($file) || !($file->isValid())) {
                return 0;
            } else {
                if (($handle = fopen($file->getRealPath(), "r")) !== FALSE) {
                    $entityManager = $controller->getDoctrine()->getManager();

                    $i = 0;
                    $criteria = null;
                    $lastCriteria = '';
                    while(($row = fgetcsv($handle)) !== FALSE) {
                        //Ignore first CVS row with titles.
                        if ($i == 0) {
                          $i++;
                          continue;
                        }

                        if (strlen($row[0]) > 0) {
                          $criteria = $controller->getDoctrine()->getRepository('AppBundle:Criteria')->getCriteriaByName($row[0]);
                          if (is_null($criteria)) {
                            $criteria = new Criteria();
                            $criteria->setName($row[0]);
                          }

                          $lastCriteria = $row[0];
                        } else {
                          $criteria = $controller->getDoctrine()->getRepository('AppBundle:Criteria')->getCriteriaByName($lastCriteria);
                        }

                        if(!is_null($criteria) && strlen($row[1]) > 0 && strpos($criteria->getAlternateNames(), $row[1]) == false) {
                          $criteria->setAlternateNames($criteria->getAlternateNames() . $row[1] . ",");
                        }

                        if(!is_null($criteria) && strlen($row[2]) > 0 && strpos($criteria->getPositiveRemarks(), $row[2]) == false) {
                          $criteria->setPositiveRemarks($criteria->getPositiveRemarks() . $row[2] . ",");
                        }

                        if(!is_null($criteria) && strlen($row[3]) > 0 && strpos($criteria->getNegativeRemarks(), $row[3]) == false) {
                          $criteria->setNegativeRemarks($criteria->getNegativeRemarks() . $row[3].  ",");
                        }

                        $entityManager->persist($criteria);
                        $entityManager->flush();
                    }

                    return 1;
                }
            }
         }
    }
}
