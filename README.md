base7
=====

A Symfony project created on January 5, 2017, 11:17 am.


To launch the app just type the following on your terminal:

php composer.phar install
php app/console cache:clear
php app/console assets:install
php app/console assetic:dump
php app/console doctrine:database:create
php app/console doctrine:migrations:migrate
phpunit -c app/
php app/console server:run

Then open your browser on http://localhost:8000
