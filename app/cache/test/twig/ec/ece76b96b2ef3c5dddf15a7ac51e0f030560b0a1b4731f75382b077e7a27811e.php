<?php

/* reviews/review.html.twig */
class __TwigTemplate_7a73f51ec909a74b9a68b303770282f150f4c99826fc113ec4d31511ce4a5bee extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "reviews/review.html.twig", 1);
        $this->blocks = array(
            'body_id' => array($this, 'block_body_id'),
            'main' => array($this, 'block_main'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_735bcc1ff2d699e518e4f9a688f9818d000f9cfc2054f55cb19405690e100550 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_735bcc1ff2d699e518e4f9a688f9818d000f9cfc2054f55cb19405690e100550->enter($__internal_735bcc1ff2d699e518e4f9a688f9818d000f9cfc2054f55cb19405690e100550_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "reviews/review.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_735bcc1ff2d699e518e4f9a688f9818d000f9cfc2054f55cb19405690e100550->leave($__internal_735bcc1ff2d699e518e4f9a688f9818d000f9cfc2054f55cb19405690e100550_prof);

    }

    // line 3
    public function block_body_id($context, array $blocks = array())
    {
        $__internal_cb97ebf43fa309042c5e935eb41487e552d3541224092c7930d04331e968c22b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cb97ebf43fa309042c5e935eb41487e552d3541224092c7930d04331e968c22b->enter($__internal_cb97ebf43fa309042c5e935eb41487e552d3541224092c7930d04331e968c22b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        echo "review";
        
        $__internal_cb97ebf43fa309042c5e935eb41487e552d3541224092c7930d04331e968c22b->leave($__internal_cb97ebf43fa309042c5e935eb41487e552d3541224092c7930d04331e968c22b_prof);

    }

    // line 5
    public function block_main($context, array $blocks = array())
    {
        $__internal_f66a1fb7d986b4a1d6cc3ef5b01681b3160277c43ae5b93ac73257064533c5fd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f66a1fb7d986b4a1d6cc3ef5b01681b3160277c43ae5b93ac73257064533c5fd->enter($__internal_f66a1fb7d986b4a1d6cc3ef5b01681b3160277c43ae5b93ac73257064533c5fd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main"));

        // line 6
        echo "    <h1>Reviews</h1>
    <div class=\"item-actions\">
        <div class=\"btn\">
          <tr>
              <a href=\"";
        // line 10
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("criteria");
        echo "\" class=\"btn-link\">
                  <i class=\"fa fa-plus\"></i>Check Criteria
              </a>
          </tr>
        </div>
    </div>

    <table class=\"table table-striped\">
        <thead>
            <tr>
                <th><i class=\"fa fa-user\"></i>Stock Id</th>
                <th><i class=\"fa fa-user\"></i>Review Id</th>
                <th><i class=\"fa fa-user\"></i>Text</th>
                <th><i class=\"fa fa-user\"></i>Score</th>
                <th><i class=\"fa fa-user\"></i>Total</th>
                <th><i class=\"fa fa-cogs\"></i>Actions</th>
            </tr>
        </thead>
        <tbody>
        ";
        // line 29
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["reviews"]) ? $context["reviews"] : $this->getContext($context, "reviews")));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["review"]) {
            // line 30
            echo "            <tr>
                <td>";
            // line 31
            echo twig_escape_filter($this->env, $this->getAttribute($context["review"], "stockId", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 32
            echo twig_escape_filter($this->env, $this->getAttribute($context["review"], "reviewId", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 33
            echo twig_escape_filter($this->env, $this->getAttribute($context["review"], "reviewText", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 34
            echo twig_escape_filter($this->env, $this->getAttribute($context["review"], "score", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 35
            echo twig_escape_filter($this->env, $this->getAttribute($context["review"], "total", array()), "html", null, true);
            echo "</td>
                <td>
                    <div class=\"item-actions\">
                        <a href=\"";
            // line 38
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("eval_review", array("id" => $this->getAttribute($context["review"], "id", array()))), "html", null, true);
            echo "\" class=\"btn btn-sm btn-primary\">
                            <i class=\"fa fa-edit\"></i> Evaluate
                        </a>
                        <a href=\"";
            // line 41
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("modify_review", array("id" => $this->getAttribute($context["review"], "id", array()))), "html", null, true);
            echo "\" class=\"btn btn-sm btn-default\">
                            <i class=\"fa fa-edit\"></i>Modify
                        </a>
                        <a href=\"";
            // line 44
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("delete_review", array("id" => $this->getAttribute($context["review"], "id", array()))), "html", null, true);
            echo "\" class=\"btn btn-sm btn-default\">
                            <i class=\"fa fa-edit\"></i>Delete
                        </a>
                    </div>
                </td>
            </tr>
        ";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 51
            echo "            <tr>
                <td colspan=\"6\" align=\"center\">Not reviews added.</td>
           </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['review'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 55
        echo "        </tbody>
    </table>
    <br />
    <div class=\"item-actions\">
        <div class=\"btn\" colspan=\"8\">
          <tr>
              <a href=\"";
        // line 61
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("add_review");
        echo "\" class=\"btn btn-sm btn-default\">
                  <i class=\"fa fa-plus\"></i>Add a review
              </a>
          </tr>
        </div>
        <div class=\"btn btn-sm btn-primary\">
            <tr>
                <td colspan=\"4\" align=\"center\">Load Review CSV File</td>
                <form action=\"";
        // line 69
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("upload_review_file");
        echo "\" method=\"post\" enctype=\"multipart/form-data\">
                      <input type=\"file\" name=\"reviewFile\" id=\"reviewFile\">
                      <button class=\"fa fa-plus btn-default\" type=\"submit\">Upload</button>
                </form>
            </tr>
        </div>
    </div>

    <div class=\"navigation text-center\">
        ";
        // line 78
        echo $this->env->getExtension('WhiteOctober\PagerfantaBundle\Twig\PagerfantaExtension')->renderPagerfanta((isset($context["reviews"]) ? $context["reviews"] : $this->getContext($context, "reviews")), "twitter_bootstrap3", array("routeName" => "homepage_paginated"));
        echo "
    </div>

";
        
        $__internal_f66a1fb7d986b4a1d6cc3ef5b01681b3160277c43ae5b93ac73257064533c5fd->leave($__internal_f66a1fb7d986b4a1d6cc3ef5b01681b3160277c43ae5b93ac73257064533c5fd_prof);

    }

    public function getTemplateName()
    {
        return "reviews/review.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  175 => 78,  163 => 69,  152 => 61,  144 => 55,  135 => 51,  123 => 44,  117 => 41,  111 => 38,  105 => 35,  101 => 34,  97 => 33,  93 => 32,  89 => 31,  86 => 30,  81 => 29,  59 => 10,  53 => 6,  47 => 5,  35 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body_id 'review' %}

{% block main %}
    <h1>Reviews</h1>
    <div class=\"item-actions\">
        <div class=\"btn\">
          <tr>
              <a href=\"{{ path('criteria') }}\" class=\"btn-link\">
                  <i class=\"fa fa-plus\"></i>Check Criteria
              </a>
          </tr>
        </div>
    </div>

    <table class=\"table table-striped\">
        <thead>
            <tr>
                <th><i class=\"fa fa-user\"></i>Stock Id</th>
                <th><i class=\"fa fa-user\"></i>Review Id</th>
                <th><i class=\"fa fa-user\"></i>Text</th>
                <th><i class=\"fa fa-user\"></i>Score</th>
                <th><i class=\"fa fa-user\"></i>Total</th>
                <th><i class=\"fa fa-cogs\"></i>Actions</th>
            </tr>
        </thead>
        <tbody>
        {% for review in reviews %}
            <tr>
                <td>{{ review.stockId }}</td>
                <td>{{ review.reviewId }}</td>
                <td>{{ review.reviewText }}</td>
                <td>{{ review.score }}</td>
                <td>{{ review.total }}</td>
                <td>
                    <div class=\"item-actions\">
                        <a href=\"{{ path('eval_review', { id: review.id }) }}\" class=\"btn btn-sm btn-primary\">
                            <i class=\"fa fa-edit\"></i> Evaluate
                        </a>
                        <a href=\"{{ path('modify_review', { id: review.id }) }}\" class=\"btn btn-sm btn-default\">
                            <i class=\"fa fa-edit\"></i>Modify
                        </a>
                        <a href=\"{{ path('delete_review', { id: review.id }) }}\" class=\"btn btn-sm btn-default\">
                            <i class=\"fa fa-edit\"></i>Delete
                        </a>
                    </div>
                </td>
            </tr>
        {% else %}
            <tr>
                <td colspan=\"6\" align=\"center\">Not reviews added.</td>
           </tr>
        {% endfor %}
        </tbody>
    </table>
    <br />
    <div class=\"item-actions\">
        <div class=\"btn\" colspan=\"8\">
          <tr>
              <a href=\"{{ path('add_review') }}\" class=\"btn btn-sm btn-default\">
                  <i class=\"fa fa-plus\"></i>Add a review
              </a>
          </tr>
        </div>
        <div class=\"btn btn-sm btn-primary\">
            <tr>
                <td colspan=\"4\" align=\"center\">Load Review CSV File</td>
                <form action=\"{{ path('upload_review_file') }}\" method=\"post\" enctype=\"multipart/form-data\">
                      <input type=\"file\" name=\"reviewFile\" id=\"reviewFile\">
                      <button class=\"fa fa-plus btn-default\" type=\"submit\">Upload</button>
                </form>
            </tr>
        </div>
    </div>

    <div class=\"navigation text-center\">
        {{ pagerfanta(reviews, 'twitter_bootstrap3', { routeName: 'homepage_paginated' }) }}
    </div>

{% endblock %}
", "reviews/review.html.twig", "/mnt/499689A1280E35AE/Feina/Base7Booking Application/base7/app/Resources/views/reviews/review.html.twig");
    }
}
