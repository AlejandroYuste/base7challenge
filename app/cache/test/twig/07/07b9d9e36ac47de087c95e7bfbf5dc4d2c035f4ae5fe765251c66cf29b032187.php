<?php

/* @Twig/Exception/exception_full.html.twig */
class __TwigTemplate_e7d23a4b317df066f3ab057b985d60b83318ae336959245f3bc4b75bb0e4a73a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@Twig/Exception/exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_523af60ff016d3576db86179a4f209cd41eb3212876a778c3c2e0706e153a80a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_523af60ff016d3576db86179a4f209cd41eb3212876a778c3c2e0706e153a80a->enter($__internal_523af60ff016d3576db86179a4f209cd41eb3212876a778c3c2e0706e153a80a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_523af60ff016d3576db86179a4f209cd41eb3212876a778c3c2e0706e153a80a->leave($__internal_523af60ff016d3576db86179a4f209cd41eb3212876a778c3c2e0706e153a80a_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_334ff2a64ae70c26b8eda2e854b1e3704c2b085c987dc6389277747b81484b46 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_334ff2a64ae70c26b8eda2e854b1e3704c2b085c987dc6389277747b81484b46->enter($__internal_334ff2a64ae70c26b8eda2e854b1e3704c2b085c987dc6389277747b81484b46_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\HttpFoundationExtension')->generateAbsoluteUrl($this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/framework/css/exception.css")), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
";
        
        $__internal_334ff2a64ae70c26b8eda2e854b1e3704c2b085c987dc6389277747b81484b46->leave($__internal_334ff2a64ae70c26b8eda2e854b1e3704c2b085c987dc6389277747b81484b46_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_363b1f377b82c9552e1e4b4dfaba1ed999962b31f32744f3b53335a7dc750787 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_363b1f377b82c9552e1e4b4dfaba1ed999962b31f32744f3b53335a7dc750787->enter($__internal_363b1f377b82c9552e1e4b4dfaba1ed999962b31f32744f3b53335a7dc750787_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_363b1f377b82c9552e1e4b4dfaba1ed999962b31f32744f3b53335a7dc750787->leave($__internal_363b1f377b82c9552e1e4b4dfaba1ed999962b31f32744f3b53335a7dc750787_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_9a6b919bebb1a812a7d8fbecb3275e4816dd4a5f191fdf89c27ffd7041f1adfa = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9a6b919bebb1a812a7d8fbecb3275e4816dd4a5f191fdf89c27ffd7041f1adfa->enter($__internal_9a6b919bebb1a812a7d8fbecb3275e4816dd4a5f191fdf89c27ffd7041f1adfa_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "@Twig/Exception/exception_full.html.twig", 12)->display($context);
        
        $__internal_9a6b919bebb1a812a7d8fbecb3275e4816dd4a5f191fdf89c27ffd7041f1adfa->leave($__internal_9a6b919bebb1a812a7d8fbecb3275e4816dd4a5f191fdf89c27ffd7041f1adfa_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 12,  72 => 11,  58 => 8,  52 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@Twig/layout.html.twig' %}

{% block head %}
    <link href=\"{{ absolute_url(asset('bundles/framework/css/exception.css')) }}\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
{% endblock %}

{% block title %}
    {{ exception.message }} ({{ status_code }} {{ status_text }})
{% endblock %}

{% block body %}
    {% include '@Twig/Exception/exception.html.twig' %}
{% endblock %}
", "@Twig/Exception/exception_full.html.twig", "/mnt/499689A1280E35AE/Feina/Base7Booking Application/base7/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception_full.html.twig");
    }
}
