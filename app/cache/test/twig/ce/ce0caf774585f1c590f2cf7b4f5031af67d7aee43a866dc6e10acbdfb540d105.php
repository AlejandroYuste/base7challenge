<?php

/* criteria/criteria.html.twig */
class __TwigTemplate_6deb7b9a86c8ed8cc61b94a83c2651e7ab8bc3a8360d870ba33c7496ae6a7ac6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "criteria/criteria.html.twig", 1);
        $this->blocks = array(
            'body_id' => array($this, 'block_body_id'),
            'main' => array($this, 'block_main'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0e6fd7e1b3f7ba673c09ea78c0ad34248f0d583c1c90d442ddb8fad572f5782e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0e6fd7e1b3f7ba673c09ea78c0ad34248f0d583c1c90d442ddb8fad572f5782e->enter($__internal_0e6fd7e1b3f7ba673c09ea78c0ad34248f0d583c1c90d442ddb8fad572f5782e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "criteria/criteria.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_0e6fd7e1b3f7ba673c09ea78c0ad34248f0d583c1c90d442ddb8fad572f5782e->leave($__internal_0e6fd7e1b3f7ba673c09ea78c0ad34248f0d583c1c90d442ddb8fad572f5782e_prof);

    }

    // line 3
    public function block_body_id($context, array $blocks = array())
    {
        $__internal_c7ca7fd1dff1f3e9259e58e83c45569ca36641b380f06c9e588554e5f4bef82f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c7ca7fd1dff1f3e9259e58e83c45569ca36641b380f06c9e588554e5f4bef82f->enter($__internal_c7ca7fd1dff1f3e9259e58e83c45569ca36641b380f06c9e588554e5f4bef82f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        echo "criteria";
        
        $__internal_c7ca7fd1dff1f3e9259e58e83c45569ca36641b380f06c9e588554e5f4bef82f->leave($__internal_c7ca7fd1dff1f3e9259e58e83c45569ca36641b380f06c9e588554e5f4bef82f_prof);

    }

    // line 5
    public function block_main($context, array $blocks = array())
    {
        $__internal_9e7dc020854a54c8c097b7743388285d6b59a4b4213bce26fec41f369ecd4954 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9e7dc020854a54c8c097b7743388285d6b59a4b4213bce26fec41f369ecd4954->enter($__internal_9e7dc020854a54c8c097b7743388285d6b59a4b4213bce26fec41f369ecd4954_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main"));

        // line 6
        echo "    <h1>Criteria</h1>
    <div class=\"item-actions\">
        <div class=\"btn\">
          <tr>
              <a href=\"";
        // line 10
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("homepage");
        echo "\" class=\"btn-link\">
                  <i class=\"fa fa-plus\"></i>Check Reviews
              </a>
          </tr>
        </div>
    </div>

    <table class=\"table table-striped\">
        <thead>
            <tr>
                <th><i class=\"fa fa-user\"></i>Name</th>
                <th><i class=\"fa fa-user\"></i>Alternate Names</th>
                <th><i class=\"fa fa-user\"></i>Positive Remarks</th>
                <th><i class=\"fa fa-user\"></i>Negative Remarks</th>
                <th><i class=\"fa fa-cogs\"></i>Actions</th>
            </tr>
        </thead>
        <tbody>
        ";
        // line 28
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["criteria"]) ? $context["criteria"] : $this->getContext($context, "criteria")));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["crit"]) {
            // line 29
            echo "            <tr>
                <td>";
            // line 30
            echo twig_escape_filter($this->env, $this->getAttribute($context["crit"], "name", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 31
            echo twig_escape_filter($this->env, $this->getAttribute($context["crit"], "alternateNames", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 32
            echo twig_escape_filter($this->env, $this->getAttribute($context["crit"], "positiveRemarks", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 33
            echo twig_escape_filter($this->env, $this->getAttribute($context["crit"], "negativeRemarks", array()), "html", null, true);
            echo "</td>
                <td>
                    <div class=\"item-actions\">
                        <a href=\"";
            // line 36
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("modify_criteria", array("id" => $this->getAttribute($context["crit"], "id", array()))), "html", null, true);
            echo "\" class=\"btn btn-sm btn-default\">
                            <i class=\"fa fa-edit\"></i>Modify
                        </a>
                        <a href=\"";
            // line 39
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("delete_criteria", array("id" => $this->getAttribute($context["crit"], "id", array()))), "html", null, true);
            echo "\" class=\"btn btn-sm btn-default\">
                            <i class=\"fa fa-edit\"></i>Delete
                        </a>
                    </div>
                </td>
            </tr>
        ";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 46
            echo "            <tr>
                <td colspan=\"6\" align=\"center\">Not criteria added.</td>
           </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['crit'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 50
        echo "        </tbody>
    </table>
    <br />
    <div class=\"item-actions\">
        <div class=\"btn\">
          <tr>
              <a href=\"";
        // line 56
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("add_criteria");
        echo "\" class=\"btn btn-sm btn-default\">
                  <i class=\"fa fa-plus\"></i>Add criteria
              </a>
          </tr>
        </div>
        <div class=\"btn btn-sm btn-primary\">
            <tr>
                <td colspan=\"4\" align=\"center\">Load Criteria CSV File</td>
                <form action=\"";
        // line 64
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("upload_criteria_file");
        echo "\" method=\"post\" enctype=\"multipart/form-data\">
                      <input type=\"file\" name=\"criteriaFile\" id=\"criteriaFile\">
                      <button class=\"fa fa-plus btn-default\" type=\"submit\">Upload</button>
                </form>
            </tr>
        </div>
    </div>

    <div class=\"navigation text-center\">
        ";
        // line 73
        echo $this->env->getExtension('WhiteOctober\PagerfantaBundle\Twig\PagerfantaExtension')->renderPagerfanta((isset($context["criteria"]) ? $context["criteria"] : $this->getContext($context, "criteria")), "twitter_bootstrap3", array("routeName" => "criteria_paginated"));
        echo "
    </div>

";
        
        $__internal_9e7dc020854a54c8c097b7743388285d6b59a4b4213bce26fec41f369ecd4954->leave($__internal_9e7dc020854a54c8c097b7743388285d6b59a4b4213bce26fec41f369ecd4954_prof);

    }

    public function getTemplateName()
    {
        return "criteria/criteria.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  164 => 73,  152 => 64,  141 => 56,  133 => 50,  124 => 46,  112 => 39,  106 => 36,  100 => 33,  96 => 32,  92 => 31,  88 => 30,  85 => 29,  80 => 28,  59 => 10,  53 => 6,  47 => 5,  35 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body_id 'criteria' %}

{% block main %}
    <h1>Criteria</h1>
    <div class=\"item-actions\">
        <div class=\"btn\">
          <tr>
              <a href=\"{{ path('homepage') }}\" class=\"btn-link\">
                  <i class=\"fa fa-plus\"></i>Check Reviews
              </a>
          </tr>
        </div>
    </div>

    <table class=\"table table-striped\">
        <thead>
            <tr>
                <th><i class=\"fa fa-user\"></i>Name</th>
                <th><i class=\"fa fa-user\"></i>Alternate Names</th>
                <th><i class=\"fa fa-user\"></i>Positive Remarks</th>
                <th><i class=\"fa fa-user\"></i>Negative Remarks</th>
                <th><i class=\"fa fa-cogs\"></i>Actions</th>
            </tr>
        </thead>
        <tbody>
        {% for crit in criteria %}
            <tr>
                <td>{{ crit.name }}</td>
                <td>{{ crit.alternateNames }}</td>
                <td>{{ crit.positiveRemarks }}</td>
                <td>{{ crit.negativeRemarks }}</td>
                <td>
                    <div class=\"item-actions\">
                        <a href=\"{{ path('modify_criteria', { id: crit.id }) }}\" class=\"btn btn-sm btn-default\">
                            <i class=\"fa fa-edit\"></i>Modify
                        </a>
                        <a href=\"{{ path('delete_criteria', { id: crit.id }) }}\" class=\"btn btn-sm btn-default\">
                            <i class=\"fa fa-edit\"></i>Delete
                        </a>
                    </div>
                </td>
            </tr>
        {% else %}
            <tr>
                <td colspan=\"6\" align=\"center\">Not criteria added.</td>
           </tr>
        {% endfor %}
        </tbody>
    </table>
    <br />
    <div class=\"item-actions\">
        <div class=\"btn\">
          <tr>
              <a href=\"{{ path('add_criteria') }}\" class=\"btn btn-sm btn-default\">
                  <i class=\"fa fa-plus\"></i>Add criteria
              </a>
          </tr>
        </div>
        <div class=\"btn btn-sm btn-primary\">
            <tr>
                <td colspan=\"4\" align=\"center\">Load Criteria CSV File</td>
                <form action=\"{{ path('upload_criteria_file') }}\" method=\"post\" enctype=\"multipart/form-data\">
                      <input type=\"file\" name=\"criteriaFile\" id=\"criteriaFile\">
                      <button class=\"fa fa-plus btn-default\" type=\"submit\">Upload</button>
                </form>
            </tr>
        </div>
    </div>

    <div class=\"navigation text-center\">
        {{ pagerfanta(criteria, 'twitter_bootstrap3', { routeName: 'criteria_paginated' }) }}
    </div>

{% endblock %}
", "criteria/criteria.html.twig", "/mnt/499689A1280E35AE/Feina/Base7Booking Application/base7/app/Resources/views/criteria/criteria.html.twig");
    }
}
