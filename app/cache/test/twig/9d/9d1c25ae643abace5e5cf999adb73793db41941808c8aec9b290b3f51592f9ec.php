<?php

/* base.html.twig */
class __TwigTemplate_0af4db642e2cfc7110b72fc0ed13d8765be237c544d39f2db788c04d3b85adcf extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body_id' => array($this, 'block_body_id'),
            'header' => array($this, 'block_header'),
            'header_navigation_links' => array($this, 'block_header_navigation_links'),
            'body' => array($this, 'block_body'),
            'main' => array($this, 'block_main'),
            'footer' => array($this, 'block_footer'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_3ba9fe57b8f938b5e851b1cc96a10a5f218f52c9dbfd848fc8d00355e37d2daf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3ba9fe57b8f938b5e851b1cc96a10a5f218f52c9dbfd848fc8d00355e37d2daf->enter($__internal_3ba9fe57b8f938b5e851b1cc96a10a5f218f52c9dbfd848fc8d00355e37d2daf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>

        ";
        // line 7
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 19
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>

    <body id=\"";
        // line 22
        $this->displayBlock('body_id', $context, $blocks);
        echo "\">
        ";
        // line 23
        $this->displayBlock('header', $context, $blocks);
        // line 56
        echo "
        <div class=\"container body-container\">
            ";
        // line 58
        $this->displayBlock('body', $context, $blocks);
        // line 66
        echo "        </div>

        ";
        // line 68
        $this->displayBlock('footer', $context, $blocks);
        // line 87
        echo "
        ";
        // line 88
        $this->displayBlock('javascripts', $context, $blocks);
        // line 101
        echo "    </body>
</html>
";
        
        $__internal_3ba9fe57b8f938b5e851b1cc96a10a5f218f52c9dbfd848fc8d00355e37d2daf->leave($__internal_3ba9fe57b8f938b5e851b1cc96a10a5f218f52c9dbfd848fc8d00355e37d2daf_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_ce136594afdcef314fe0f769bbd41757df8888f5243ec5d26d8fd45d40d4cb25 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ce136594afdcef314fe0f769bbd41757df8888f5243ec5d26d8fd45d40d4cb25->enter($__internal_ce136594afdcef314fe0f769bbd41757df8888f5243ec5d26d8fd45d40d4cb25_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Base7 Backend Challenge.";
        
        $__internal_ce136594afdcef314fe0f769bbd41757df8888f5243ec5d26d8fd45d40d4cb25->leave($__internal_ce136594afdcef314fe0f769bbd41757df8888f5243ec5d26d8fd45d40d4cb25_prof);

    }

    // line 7
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_9914414b5dad48da9c3a12e3cce661171670762f56c55af221dd0939c0374b4b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9914414b5dad48da9c3a12e3cce661171670762f56c55af221dd0939c0374b4b->enter($__internal_9914414b5dad48da9c3a12e3cce661171670762f56c55af221dd0939c0374b4b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 8
        echo "            ";
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "17e7ee5_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_17e7ee5_0") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("_controller/css/app_bootstrap-flatly_1.css");
            // line 14
            echo "                <link rel=\"stylesheet\" Shref=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
            ";
            // asset "17e7ee5_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_17e7ee5_1") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("_controller/css/app_font-awesome_2.css");
            echo "                <link rel=\"stylesheet\" Shref=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
            ";
            // asset "17e7ee5_2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_17e7ee5_2") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("_controller/css/app_part_3_bootstrap-datetimepicker.min_1.css");
            echo "                <link rel=\"stylesheet\" Shref=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
            ";
            // asset "17e7ee5_3"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_17e7ee5_3") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("_controller/css/app_part_3_font-lato_2.css");
            echo "                <link rel=\"stylesheet\" Shref=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
            ";
            // asset "17e7ee5_4"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_17e7ee5_4") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("_controller/css/app_part_3_highlight-solarized-light_3.css");
            echo "                <link rel=\"stylesheet\" Shref=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
            ";
            // asset "17e7ee5_5"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_17e7ee5_5") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("_controller/css/app_main_4.css");
            echo "                <link rel=\"stylesheet\" Shref=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
            ";
        } else {
            // asset "17e7ee5"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_17e7ee5") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("_controller/css/app.css");
            echo "                <link rel=\"stylesheet\" Shref=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
            ";
        }
        unset($context["asset_url"]);
        // line 16
        echo "
             <link rel=\"stylesheet\" href=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/app.css"), "html", null, true);
        echo "\">
        ";
        
        $__internal_9914414b5dad48da9c3a12e3cce661171670762f56c55af221dd0939c0374b4b->leave($__internal_9914414b5dad48da9c3a12e3cce661171670762f56c55af221dd0939c0374b4b_prof);

    }

    // line 22
    public function block_body_id($context, array $blocks = array())
    {
        $__internal_3044ed57e3c3960c919ec0226ab084c06666cd097bb3b74c85c2aaa760b0a229 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3044ed57e3c3960c919ec0226ab084c06666cd097bb3b74c85c2aaa760b0a229->enter($__internal_3044ed57e3c3960c919ec0226ab084c06666cd097bb3b74c85c2aaa760b0a229_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        
        $__internal_3044ed57e3c3960c919ec0226ab084c06666cd097bb3b74c85c2aaa760b0a229->leave($__internal_3044ed57e3c3960c919ec0226ab084c06666cd097bb3b74c85c2aaa760b0a229_prof);

    }

    // line 23
    public function block_header($context, array $blocks = array())
    {
        $__internal_bb4f3ff8e1e9dcc9403124e083285b749172e2925e9b9fe0e792d5012c6ef551 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bb4f3ff8e1e9dcc9403124e083285b749172e2925e9b9fe0e792d5012c6ef551->enter($__internal_bb4f3ff8e1e9dcc9403124e083285b749172e2925e9b9fe0e792d5012c6ef551_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header"));

        // line 24
        echo "            <header>
                <div class=\"navbar navbar-default navbar-static-top\" role=\"navigation\">
                    <div class=\"container\">
                        <div class=\"navbar-header\">
                            <a class=\"navbar-brand\" href=\"";
        // line 28
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("homepage");
        echo "\">
                                Base7Booking BE Challenge
                            </a>

                            <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-collapse\">
                                <span class=\"sr-only\">Toggle navigation</span>
                                <span class=\"icon-bar\"></span>
                                <span class=\"icon-bar\"></span>
                                <span class=\"icon-bar\"></span>
                            </button>
                        </div>
                        <div class=\"navbar-collapse collapse\">
                            <ul class=\"nav navbar-nav navbar-right\">

                                ";
        // line 42
        $this->displayBlock('header_navigation_links', $context, $blocks);
        // line 49
        echo "
                            </ul>
                        </div>
                    </div>
                </div>
            </header>
        ";
        
        $__internal_bb4f3ff8e1e9dcc9403124e083285b749172e2925e9b9fe0e792d5012c6ef551->leave($__internal_bb4f3ff8e1e9dcc9403124e083285b749172e2925e9b9fe0e792d5012c6ef551_prof);

    }

    // line 42
    public function block_header_navigation_links($context, array $blocks = array())
    {
        $__internal_56abd4e9edb3ff5b5c356f2f3b4c1bf1cad54338610115996f352b0127065826 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_56abd4e9edb3ff5b5c356f2f3b4c1bf1cad54338610115996f352b0127065826->enter($__internal_56abd4e9edb3ff5b5c356f2f3b4c1bf1cad54338610115996f352b0127065826_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header_navigation_links"));

        // line 43
        echo "                                    <li>
                                        <a href=\"";
        // line 44
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("homepage");
        echo "\">
                                            <i class=\"fa fa-home\" /></i> Home
                                        </a>
                                    </li>
                                ";
        
        $__internal_56abd4e9edb3ff5b5c356f2f3b4c1bf1cad54338610115996f352b0127065826->leave($__internal_56abd4e9edb3ff5b5c356f2f3b4c1bf1cad54338610115996f352b0127065826_prof);

    }

    // line 58
    public function block_body($context, array $blocks = array())
    {
        $__internal_c3e20c15b43167dbb94a1fa9310f6e3aa776b0f5787d5dd9fa004ca5049f2225 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c3e20c15b43167dbb94a1fa9310f6e3aa776b0f5787d5dd9fa004ca5049f2225->enter($__internal_c3e20c15b43167dbb94a1fa9310f6e3aa776b0f5787d5dd9fa004ca5049f2225_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 59
        echo "                <div class=\"row\">
                    <div id=\"main\" class=\"col-sm-12\">
                        ";
        // line 61
        $this->displayBlock('main', $context, $blocks);
        // line 63
        echo "                    </div>
                </div>
            ";
        
        $__internal_c3e20c15b43167dbb94a1fa9310f6e3aa776b0f5787d5dd9fa004ca5049f2225->leave($__internal_c3e20c15b43167dbb94a1fa9310f6e3aa776b0f5787d5dd9fa004ca5049f2225_prof);

    }

    // line 61
    public function block_main($context, array $blocks = array())
    {
        $__internal_12ba4f0c5cadfaf51f665b0bb5c774ea29776b4991b9dc6703d81327f701d1c2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_12ba4f0c5cadfaf51f665b0bb5c774ea29776b4991b9dc6703d81327f701d1c2->enter($__internal_12ba4f0c5cadfaf51f665b0bb5c774ea29776b4991b9dc6703d81327f701d1c2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main"));

        // line 62
        echo "                        ";
        
        $__internal_12ba4f0c5cadfaf51f665b0bb5c774ea29776b4991b9dc6703d81327f701d1c2->leave($__internal_12ba4f0c5cadfaf51f665b0bb5c774ea29776b4991b9dc6703d81327f701d1c2_prof);

    }

    // line 68
    public function block_footer($context, array $blocks = array())
    {
        $__internal_28f420ff65bec1e65dff4a660f9b2d4bf18a8ee85616075d2477358173dc6e26 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_28f420ff65bec1e65dff4a660f9b2d4bf18a8ee85616075d2477358173dc6e26->enter($__internal_28f420ff65bec1e65dff4a660f9b2d4bf18a8ee85616075d2477358173dc6e26_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "footer"));

        // line 69
        echo "            <footer>
                <div class=\"container\">
                    <div class=\"row\">
                        <div id=\"footer-copyright\" class=\"col-md-6\">
                            <p>&copy; ";
        // line 73
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, "now", "Y"), "html", null, true);
        echo " - The Symfony Project</p>
                            <p>MIT License</p>
                        </div>
                        <div id=\"footer-resources\" class=\"col-md-6\">
                            <p>
                                <a href=\"https://twitter.com/symfony\"><i class=\"fa fa-twitter\"></i></a>
                                <a href=\"https://www.facebook.com/SensioLabs\"><i class=\"fa fa-facebook\"></i></a>
                                <a href=\"http://symfony.com/blog\"><i class=\"fa fa-rss\"></i></a>
                            </p>
                        </div>
                    </div>
                </div>
            </footer>
        ";
        
        $__internal_28f420ff65bec1e65dff4a660f9b2d4bf18a8ee85616075d2477358173dc6e26->leave($__internal_28f420ff65bec1e65dff4a660f9b2d4bf18a8ee85616075d2477358173dc6e26_prof);

    }

    // line 88
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_225fce6c10567d822d2e024af756fe3c0793cfcb8778967f1a21565118fe33a3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_225fce6c10567d822d2e024af756fe3c0793cfcb8778967f1a21565118fe33a3->enter($__internal_225fce6c10567d822d2e024af756fe3c0793cfcb8778967f1a21565118fe33a3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 89
        echo "            ";
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "cf95333_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_cf95333_0") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("_controller/js/app_jquery-2.1.4_1.js");
            // line 96
            echo "                <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
            ";
            // asset "cf95333_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_cf95333_1") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("_controller/js/app_moment.min_2.js");
            echo "                <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
            ";
            // asset "cf95333_2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_cf95333_2") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("_controller/js/app_bootstrap-3.3.4_3.js");
            echo "                <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
            ";
            // asset "cf95333_3"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_cf95333_3") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("_controller/js/app_highlight.pack_4.js");
            echo "                <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
            ";
            // asset "cf95333_4"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_cf95333_4") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("_controller/js/app_bootstrap-datetimepicker.min_5.js");
            echo "                <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
            ";
            // asset "cf95333_5"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_cf95333_5") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("_controller/js/app_main_6.js");
            echo "                <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
            ";
        } else {
            // asset "cf95333"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_cf95333") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("_controller/js/app.js");
            echo "                <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
            ";
        }
        unset($context["asset_url"]);
        // line 98
        echo "
            <script src=\"";
        // line 99
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/app.js"), "html", null, true);
        echo "\"></script>
        ";
        
        $__internal_225fce6c10567d822d2e024af756fe3c0793cfcb8778967f1a21565118fe33a3->leave($__internal_225fce6c10567d822d2e024af756fe3c0793cfcb8778967f1a21565118fe33a3_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  363 => 99,  360 => 98,  316 => 96,  311 => 89,  305 => 88,  284 => 73,  278 => 69,  272 => 68,  265 => 62,  259 => 61,  250 => 63,  248 => 61,  244 => 59,  238 => 58,  226 => 44,  223 => 43,  217 => 42,  204 => 49,  202 => 42,  185 => 28,  179 => 24,  173 => 23,  162 => 22,  153 => 17,  150 => 16,  106 => 14,  101 => 8,  95 => 7,  83 => 5,  74 => 101,  72 => 88,  69 => 87,  67 => 68,  63 => 66,  61 => 58,  57 => 56,  55 => 23,  51 => 22,  44 => 19,  42 => 7,  37 => 5,  31 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>{% block title %}Base7 Backend Challenge.{% endblock %}</title>

        {% block stylesheets %}
            {% stylesheets filter=\"scssphp\" output=\"css/app.css\"
                \"%kernel.root_dir%/Resources/assets/scss/bootstrap-flatly.scss\"
                \"%kernel.root_dir%/Resources/assets/scss/font-awesome.scss\"
                \"%kernel.root_dir%/Resources/assets/css/*.css\"
                \"%kernel.root_dir%/Resources/assets/scss/main.scss\"
            %}
                <link rel=\"stylesheet\" Shref=\"{{ asset_url }}\" />
            {% endstylesheets %}

             <link rel=\"stylesheet\" href=\"{{ asset('css/app.css') }}\">
        {% endblock %}
        <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.ico') }}\" />
    </head>

    <body id=\"{% block body_id %}{% endblock %}\">
        {% block header %}
            <header>
                <div class=\"navbar navbar-default navbar-static-top\" role=\"navigation\">
                    <div class=\"container\">
                        <div class=\"navbar-header\">
                            <a class=\"navbar-brand\" href=\"{{ path('homepage') }}\">
                                Base7Booking BE Challenge
                            </a>

                            <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-collapse\">
                                <span class=\"sr-only\">Toggle navigation</span>
                                <span class=\"icon-bar\"></span>
                                <span class=\"icon-bar\"></span>
                                <span class=\"icon-bar\"></span>
                            </button>
                        </div>
                        <div class=\"navbar-collapse collapse\">
                            <ul class=\"nav navbar-nav navbar-right\">

                                {% block header_navigation_links %}
                                    <li>
                                        <a href=\"{{ path('homepage') }}\">
                                            <i class=\"fa fa-home\" /></i> Home
                                        </a>
                                    </li>
                                {% endblock %}

                            </ul>
                        </div>
                    </div>
                </div>
            </header>
        {% endblock %}

        <div class=\"container body-container\">
            {% block body %}
                <div class=\"row\">
                    <div id=\"main\" class=\"col-sm-12\">
                        {% block main %}
                        {% endblock %}
                    </div>
                </div>
            {% endblock %}
        </div>

        {% block footer %}
            <footer>
                <div class=\"container\">
                    <div class=\"row\">
                        <div id=\"footer-copyright\" class=\"col-md-6\">
                            <p>&copy; {{ 'now'|date('Y') }} - The Symfony Project</p>
                            <p>MIT License</p>
                        </div>
                        <div id=\"footer-resources\" class=\"col-md-6\">
                            <p>
                                <a href=\"https://twitter.com/symfony\"><i class=\"fa fa-twitter\"></i></a>
                                <a href=\"https://www.facebook.com/SensioLabs\"><i class=\"fa fa-facebook\"></i></a>
                                <a href=\"http://symfony.com/blog\"><i class=\"fa fa-rss\"></i></a>
                            </p>
                        </div>
                    </div>
                </div>
            </footer>
        {% endblock %}

        {% block javascripts %}
            {% javascripts filter=\"?jsqueeze\" output=\"js/app.js\"
                \"%kernel.root_dir%/Resources/assets/js/jquery-2.1.4.js\"
                \"%kernel.root_dir%/Resources/assets/js/moment.min.js\"
                \"%kernel.root_dir%/Resources/assets/js/bootstrap-3.3.4.js\"
                \"%kernel.root_dir%/Resources/assets/js/highlight.pack.js\"
                \"%kernel.root_dir%/Resources/assets/js/bootstrap-datetimepicker.min.js\"
                \"%kernel.root_dir%/Resources/assets/js/main.js\" %}
                <script src=\"{{ asset_url }}\"></script>
            {% endjavascripts %}

            <script src=\"{{ asset('js/app.js') }}\"></script>
        {% endblock %}
    </body>
</html>
", "base.html.twig", "/mnt/499689A1280E35AE/Feina/Base7Booking Application/base7/app/Resources/views/base.html.twig");
    }
}
