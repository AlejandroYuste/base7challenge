<?php

/* criteria/criteria.html.twig */
class __TwigTemplate_bde150986ca1d4504c5266034daf2c0e0c7b7bba5a90c4d149726885e4f37771 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "criteria/criteria.html.twig", 1);
        $this->blocks = array(
            'body_id' => array($this, 'block_body_id'),
            'main' => array($this, 'block_main'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_37c542c7f4e21c7123f5ca4ced7e6f096fd9cac35a021d6e4bcdb4d917061d24 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_37c542c7f4e21c7123f5ca4ced7e6f096fd9cac35a021d6e4bcdb4d917061d24->enter($__internal_37c542c7f4e21c7123f5ca4ced7e6f096fd9cac35a021d6e4bcdb4d917061d24_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "criteria/criteria.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_37c542c7f4e21c7123f5ca4ced7e6f096fd9cac35a021d6e4bcdb4d917061d24->leave($__internal_37c542c7f4e21c7123f5ca4ced7e6f096fd9cac35a021d6e4bcdb4d917061d24_prof);

    }

    // line 3
    public function block_body_id($context, array $blocks = array())
    {
        $__internal_d087520b57572d89b13328a6e7d5ee52dabe4062c8d84ef187e7e033938fb817 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d087520b57572d89b13328a6e7d5ee52dabe4062c8d84ef187e7e033938fb817->enter($__internal_d087520b57572d89b13328a6e7d5ee52dabe4062c8d84ef187e7e033938fb817_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        echo "criteria";
        
        $__internal_d087520b57572d89b13328a6e7d5ee52dabe4062c8d84ef187e7e033938fb817->leave($__internal_d087520b57572d89b13328a6e7d5ee52dabe4062c8d84ef187e7e033938fb817_prof);

    }

    // line 5
    public function block_main($context, array $blocks = array())
    {
        $__internal_910c8096b010d75160030230d7d52f8e61caa0a8627351fa91b4a979dcfbe949 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_910c8096b010d75160030230d7d52f8e61caa0a8627351fa91b4a979dcfbe949->enter($__internal_910c8096b010d75160030230d7d52f8e61caa0a8627351fa91b4a979dcfbe949_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main"));

        // line 6
        echo "    <h1>Criteria</h1>
    <div class=\"item-actions\">
        <div class=\"btn\">
          <tr>
              <a href=\"";
        // line 10
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("homepage");
        echo "\" class=\"btn-link\">
                  <i class=\"fa fa-plus\"></i>Check Reviews
              </a>
          </tr>
        </div>
    </div>

    <div class=\"item-actions\">
        <div class=\"btn\">
          <tr>
              <a href=\"";
        // line 20
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("add_criteria");
        echo "\" class=\"btn btn-sm btn-default\">
                  <i class=\"fa fa-plus\"></i>Add criteria
              </a>
          </tr>
        </div>
        <div class=\"btn btn-sm btn-primary\">
            <tr>
                <td colspan=\"4\" align=\"center\">Load Criteria CSV File</td>
                <form action=\"";
        // line 28
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("upload_criteria_file");
        echo "\" method=\"post\" enctype=\"multipart/form-data\">
                      <input type=\"file\" name=\"criteriaFile\" id=\"criteriaFile\">
                      <button class=\"fa fa-plus btn-default\" type=\"submit\">Upload</button>
                </form>
            </tr>
        </div>
    </div>
    <br/>
    <table class=\"table table-striped\">
        <thead>
            <tr>
                <th><i class=\"fa fa-user\"></i>Name</th>
                <th><i class=\"fa fa-user\"></i>Alternate Names</th>
                <th><i class=\"fa fa-user\"></i>Positive Remarks</th>
                <th><i class=\"fa fa-user\"></i>Negative Remarks</th>
                <th><i class=\"fa fa-cogs\"></i>Actions</th>
            </tr>
        </thead>
        <tbody>
        ";
        // line 47
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["criteria"]) ? $context["criteria"] : $this->getContext($context, "criteria")));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["crit"]) {
            // line 48
            echo "            <tr>
                <td>";
            // line 49
            echo twig_escape_filter($this->env, $this->getAttribute($context["crit"], "name", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 50
            echo twig_escape_filter($this->env, $this->getAttribute($context["crit"], "alternateNames", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 51
            echo twig_escape_filter($this->env, $this->getAttribute($context["crit"], "positiveRemarks", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 52
            echo twig_escape_filter($this->env, $this->getAttribute($context["crit"], "negativeRemarks", array()), "html", null, true);
            echo "</td>
                <td>
                    <div class=\"item-actions\">
                        <a href=\"";
            // line 55
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("modify_criteria", array("id" => $this->getAttribute($context["crit"], "id", array()))), "html", null, true);
            echo "\" class=\"btn btn-sm btn-default\">
                            <i class=\"fa fa-edit\"></i>Modify
                        </a>
                        <a href=\"";
            // line 58
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("delete_criteria", array("id" => $this->getAttribute($context["crit"], "id", array()))), "html", null, true);
            echo "\" class=\"btn btn-sm btn-default\">
                            <i class=\"fa fa-edit\"></i>Delete
                        </a>
                    </div>
                </td>
            </tr>
        ";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 65
            echo "            <tr>
                <td colspan=\"6\" align=\"center\">Not criteria added.</td>
           </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['crit'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 69
        echo "        </tbody>
    </table>
    <br />

    <div class=\"navigation text-center\">
        ";
        // line 74
        echo $this->env->getExtension('WhiteOctober\PagerfantaBundle\Twig\PagerfantaExtension')->renderPagerfanta((isset($context["criteria"]) ? $context["criteria"] : $this->getContext($context, "criteria")), "twitter_bootstrap3", array("routeName" => "criteria_paginated"));
        echo "
    </div>

";
        
        $__internal_910c8096b010d75160030230d7d52f8e61caa0a8627351fa91b4a979dcfbe949->leave($__internal_910c8096b010d75160030230d7d52f8e61caa0a8627351fa91b4a979dcfbe949_prof);

    }

    public function getTemplateName()
    {
        return "criteria/criteria.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  165 => 74,  158 => 69,  149 => 65,  137 => 58,  131 => 55,  125 => 52,  121 => 51,  117 => 50,  113 => 49,  110 => 48,  105 => 47,  83 => 28,  72 => 20,  59 => 10,  53 => 6,  47 => 5,  35 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body_id 'criteria' %}

{% block main %}
    <h1>Criteria</h1>
    <div class=\"item-actions\">
        <div class=\"btn\">
          <tr>
              <a href=\"{{ path('homepage') }}\" class=\"btn-link\">
                  <i class=\"fa fa-plus\"></i>Check Reviews
              </a>
          </tr>
        </div>
    </div>

    <div class=\"item-actions\">
        <div class=\"btn\">
          <tr>
              <a href=\"{{ path('add_criteria') }}\" class=\"btn btn-sm btn-default\">
                  <i class=\"fa fa-plus\"></i>Add criteria
              </a>
          </tr>
        </div>
        <div class=\"btn btn-sm btn-primary\">
            <tr>
                <td colspan=\"4\" align=\"center\">Load Criteria CSV File</td>
                <form action=\"{{ path('upload_criteria_file') }}\" method=\"post\" enctype=\"multipart/form-data\">
                      <input type=\"file\" name=\"criteriaFile\" id=\"criteriaFile\">
                      <button class=\"fa fa-plus btn-default\" type=\"submit\">Upload</button>
                </form>
            </tr>
        </div>
    </div>
    <br/>
    <table class=\"table table-striped\">
        <thead>
            <tr>
                <th><i class=\"fa fa-user\"></i>Name</th>
                <th><i class=\"fa fa-user\"></i>Alternate Names</th>
                <th><i class=\"fa fa-user\"></i>Positive Remarks</th>
                <th><i class=\"fa fa-user\"></i>Negative Remarks</th>
                <th><i class=\"fa fa-cogs\"></i>Actions</th>
            </tr>
        </thead>
        <tbody>
        {% for crit in criteria %}
            <tr>
                <td>{{ crit.name }}</td>
                <td>{{ crit.alternateNames }}</td>
                <td>{{ crit.positiveRemarks }}</td>
                <td>{{ crit.negativeRemarks }}</td>
                <td>
                    <div class=\"item-actions\">
                        <a href=\"{{ path('modify_criteria', { id: crit.id }) }}\" class=\"btn btn-sm btn-default\">
                            <i class=\"fa fa-edit\"></i>Modify
                        </a>
                        <a href=\"{{ path('delete_criteria', { id: crit.id }) }}\" class=\"btn btn-sm btn-default\">
                            <i class=\"fa fa-edit\"></i>Delete
                        </a>
                    </div>
                </td>
            </tr>
        {% else %}
            <tr>
                <td colspan=\"6\" align=\"center\">Not criteria added.</td>
           </tr>
        {% endfor %}
        </tbody>
    </table>
    <br />

    <div class=\"navigation text-center\">
        {{ pagerfanta(criteria, 'twitter_bootstrap3', { routeName: 'criteria_paginated' }) }}
    </div>

{% endblock %}
", "criteria/criteria.html.twig", "/mnt/499689A1280E35AE/Feina/Base7Booking Application/base7/app/Resources/views/criteria/criteria.html.twig");
    }
}
