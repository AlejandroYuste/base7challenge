<?php

/* form/fields.html.twig */
class __TwigTemplate_86d1f884054b767639685473ae521b0aed208048e423fc261deb417c62159d7d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'app_datetimepicker_widget' => array($this, 'block_app_datetimepicker_widget'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b86612d2d108850cb66992f904653f379f9c5190401034c7c8303ef5f5ad9f99 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b86612d2d108850cb66992f904653f379f9c5190401034c7c8303ef5f5ad9f99->enter($__internal_b86612d2d108850cb66992f904653f379f9c5190401034c7c8303ef5f5ad9f99_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "form/fields.html.twig"));

        // line 7
        echo "
";
        // line 8
        $this->displayBlock('app_datetimepicker_widget', $context, $blocks);
        
        $__internal_b86612d2d108850cb66992f904653f379f9c5190401034c7c8303ef5f5ad9f99->leave($__internal_b86612d2d108850cb66992f904653f379f9c5190401034c7c8303ef5f5ad9f99_prof);

    }

    public function block_app_datetimepicker_widget($context, array $blocks = array())
    {
        $__internal_6fc33b452d05ab7cb77df6a980546fa40ddd48350a6dd4332a77383529d16e66 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6fc33b452d05ab7cb77df6a980546fa40ddd48350a6dd4332a77383529d16e66->enter($__internal_6fc33b452d05ab7cb77df6a980546fa40ddd48350a6dd4332a77383529d16e66_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "app_datetimepicker_widget"));

        // line 9
        echo "    ";
        ob_start();
        // line 10
        echo "        <div class=\"input-group date\" data-toggle=\"datetimepicker\">
            ";
        // line 11
        $this->displayBlock("datetime_widget", $context, $blocks);
        echo "
            <span class=\"input-group-addon\">
                <span class=\"fa fa-calendar\"></span>
            </span>
        </div>
    ";
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
        
        $__internal_6fc33b452d05ab7cb77df6a980546fa40ddd48350a6dd4332a77383529d16e66->leave($__internal_6fc33b452d05ab7cb77df6a980546fa40ddd48350a6dd4332a77383529d16e66_prof);

    }

    public function getTemplateName()
    {
        return "form/fields.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  44 => 11,  41 => 10,  38 => 9,  26 => 8,  23 => 7,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#
   Each field type is rendered by a template fragment, which is determined
   by the value of your getName() method and the suffix _widget.

   See http://symfony.com/doc/current/cookbook/form/create_custom_field_type.html#creating-a-template-for-the-field
#}

{% block app_datetimepicker_widget %}
    {% spaceless %}
        <div class=\"input-group date\" data-toggle=\"datetimepicker\">
            {{ block('datetime_widget') }}
            <span class=\"input-group-addon\">
                <span class=\"fa fa-calendar\"></span>
            </span>
        </div>
    {% endspaceless %}
{% endblock %}
", "form/fields.html.twig", "/mnt/499689A1280E35AE/Feina/Base7Booking Application/base7/app/Resources/views/form/fields.html.twig");
    }
}
