<?php

/* @Twig/Exception/exception_full.html.twig */
class __TwigTemplate_e4959655c49da781759357e4035e204b18bfca3e5f9ed9b450265bbddb1aee47 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@Twig/Exception/exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0b2165e1f87763b608e060192a578c393f7bb0852f92bbae35b0eae717fc52f0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0b2165e1f87763b608e060192a578c393f7bb0852f92bbae35b0eae717fc52f0->enter($__internal_0b2165e1f87763b608e060192a578c393f7bb0852f92bbae35b0eae717fc52f0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_0b2165e1f87763b608e060192a578c393f7bb0852f92bbae35b0eae717fc52f0->leave($__internal_0b2165e1f87763b608e060192a578c393f7bb0852f92bbae35b0eae717fc52f0_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_10b4c6405699476ca5d0b71fc5be9b7c3ab6c8bbf50e95510af8c803d4f3fe19 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_10b4c6405699476ca5d0b71fc5be9b7c3ab6c8bbf50e95510af8c803d4f3fe19->enter($__internal_10b4c6405699476ca5d0b71fc5be9b7c3ab6c8bbf50e95510af8c803d4f3fe19_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\HttpFoundationExtension')->generateAbsoluteUrl($this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/framework/css/exception.css")), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
";
        
        $__internal_10b4c6405699476ca5d0b71fc5be9b7c3ab6c8bbf50e95510af8c803d4f3fe19->leave($__internal_10b4c6405699476ca5d0b71fc5be9b7c3ab6c8bbf50e95510af8c803d4f3fe19_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_ed2f179c04826cab145c88b47795d3295f26a6b1464feef99e50742b6befa1d4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ed2f179c04826cab145c88b47795d3295f26a6b1464feef99e50742b6befa1d4->enter($__internal_ed2f179c04826cab145c88b47795d3295f26a6b1464feef99e50742b6befa1d4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_ed2f179c04826cab145c88b47795d3295f26a6b1464feef99e50742b6befa1d4->leave($__internal_ed2f179c04826cab145c88b47795d3295f26a6b1464feef99e50742b6befa1d4_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_2e9aada611db86799ca34d13598a9b2fa9c4269be09a73a29a7f58103f06bd50 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2e9aada611db86799ca34d13598a9b2fa9c4269be09a73a29a7f58103f06bd50->enter($__internal_2e9aada611db86799ca34d13598a9b2fa9c4269be09a73a29a7f58103f06bd50_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "@Twig/Exception/exception_full.html.twig", 12)->display($context);
        
        $__internal_2e9aada611db86799ca34d13598a9b2fa9c4269be09a73a29a7f58103f06bd50->leave($__internal_2e9aada611db86799ca34d13598a9b2fa9c4269be09a73a29a7f58103f06bd50_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 12,  72 => 11,  58 => 8,  52 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@Twig/layout.html.twig' %}

{% block head %}
    <link href=\"{{ absolute_url(asset('bundles/framework/css/exception.css')) }}\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
{% endblock %}

{% block title %}
    {{ exception.message }} ({{ status_code }} {{ status_text }})
{% endblock %}

{% block body %}
    {% include '@Twig/Exception/exception.html.twig' %}
{% endblock %}
", "@Twig/Exception/exception_full.html.twig", "/mnt/499689A1280E35AE/Feina/Base7Booking Application/base7/vendor/symfony/symfony/src/Symfony/Bundle/TwigBundle/Resources/views/Exception/exception_full.html.twig");
    }
}
