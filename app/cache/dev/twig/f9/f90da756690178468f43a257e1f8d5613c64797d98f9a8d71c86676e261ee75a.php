<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_84907e3b381fe35ec2a025bef4d991ee7d08451c7375c3bac8b3f5f183d96d96 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_868aa9d4b7a89a7029573dded3e5d9358681509155994ebf0fb965c26c161b56 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_868aa9d4b7a89a7029573dded3e5d9358681509155994ebf0fb965c26c161b56->enter($__internal_868aa9d4b7a89a7029573dded3e5d9358681509155994ebf0fb965c26c161b56_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_868aa9d4b7a89a7029573dded3e5d9358681509155994ebf0fb965c26c161b56->leave($__internal_868aa9d4b7a89a7029573dded3e5d9358681509155994ebf0fb965c26c161b56_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_6531332133c571b94941317bbdf7b98bb2420378105fb7a3fcaf9a2dc0464950 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6531332133c571b94941317bbdf7b98bb2420378105fb7a3fcaf9a2dc0464950->enter($__internal_6531332133c571b94941317bbdf7b98bb2420378105fb7a3fcaf9a2dc0464950_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_6531332133c571b94941317bbdf7b98bb2420378105fb7a3fcaf9a2dc0464950->leave($__internal_6531332133c571b94941317bbdf7b98bb2420378105fb7a3fcaf9a2dc0464950_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_41d3614543594b0058dad8200d69f708fc4feb0214f05ae2c829e34ae28d51c3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_41d3614543594b0058dad8200d69f708fc4feb0214f05ae2c829e34ae28d51c3->enter($__internal_41d3614543594b0058dad8200d69f708fc4feb0214f05ae2c829e34ae28d51c3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_41d3614543594b0058dad8200d69f708fc4feb0214f05ae2c829e34ae28d51c3->leave($__internal_41d3614543594b0058dad8200d69f708fc4feb0214f05ae2c829e34ae28d51c3_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_13a449f4b1f70ca3d9c1e1b70a197edb2d202917bac453df55c71797f7489c86 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_13a449f4b1f70ca3d9c1e1b70a197edb2d202917bac453df55c71797f7489c86->enter($__internal_13a449f4b1f70ca3d9c1e1b70a197edb2d202917bac453df55c71797f7489c86_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\HttpKernelExtension')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_router", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_13a449f4b1f70ca3d9c1e1b70a197edb2d202917bac453df55c71797f7489c86->leave($__internal_13a449f4b1f70ca3d9c1e1b70a197edb2d202917bac453df55c71797f7489c86_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 13,  67 => 12,  56 => 7,  53 => 6,  47 => 5,  36 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}{% endblock %}

{% block menu %}
<span class=\"label\">
    <span class=\"icon\">{{ include('@WebProfiler/Icon/router.svg') }}</span>
    <strong>Routing</strong>
</span>
{% endblock %}

{% block panel %}
    {{ render(path('_profiler_router', { token: token })) }}
{% endblock %}
", "@WebProfiler/Collector/router.html.twig", "/mnt/499689A1280E35AE/Feina/Base7Booking Application/base7/vendor/symfony/symfony/src/Symfony/Bundle/WebProfilerBundle/Resources/views/Collector/router.html.twig");
    }
}
