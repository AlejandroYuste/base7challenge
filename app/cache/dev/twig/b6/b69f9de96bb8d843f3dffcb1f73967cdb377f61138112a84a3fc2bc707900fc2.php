<?php

/* reviews/review.html.twig */
class __TwigTemplate_624a83fc36e754698680ecae7e5c4d8f3d82fe44570046e166ca95d2fe421c5d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "reviews/review.html.twig", 1);
        $this->blocks = array(
            'body_id' => array($this, 'block_body_id'),
            'main' => array($this, 'block_main'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1d020a9e15502a500e3fc1945d069437956667e07cbb7a5e17532894b605bbca = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_1d020a9e15502a500e3fc1945d069437956667e07cbb7a5e17532894b605bbca->enter($__internal_1d020a9e15502a500e3fc1945d069437956667e07cbb7a5e17532894b605bbca_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "reviews/review.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_1d020a9e15502a500e3fc1945d069437956667e07cbb7a5e17532894b605bbca->leave($__internal_1d020a9e15502a500e3fc1945d069437956667e07cbb7a5e17532894b605bbca_prof);

    }

    // line 3
    public function block_body_id($context, array $blocks = array())
    {
        $__internal_dde88288a54b6e5323ff954c884c25228e53e0b0c65f238e98bb5142807d8ad6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_dde88288a54b6e5323ff954c884c25228e53e0b0c65f238e98bb5142807d8ad6->enter($__internal_dde88288a54b6e5323ff954c884c25228e53e0b0c65f238e98bb5142807d8ad6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        echo "review";
        
        $__internal_dde88288a54b6e5323ff954c884c25228e53e0b0c65f238e98bb5142807d8ad6->leave($__internal_dde88288a54b6e5323ff954c884c25228e53e0b0c65f238e98bb5142807d8ad6_prof);

    }

    // line 5
    public function block_main($context, array $blocks = array())
    {
        $__internal_28da129d751571ec7796513061289bbb4d456b1f83841d2dcbc21382663fc83a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_28da129d751571ec7796513061289bbb4d456b1f83841d2dcbc21382663fc83a->enter($__internal_28da129d751571ec7796513061289bbb4d456b1f83841d2dcbc21382663fc83a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main"));

        // line 6
        echo "    <h1>Reviews</h1>
    <div class=\"item-actions\">
        <div class=\"btn\">
          <tr>
              <a href=\"";
        // line 10
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("criteria");
        echo "\" class=\"btn-link\">
                  <i class=\"fa fa-plus\"></i>Check Criteria
              </a>
          </tr>
        </div>
    </div>
    <div class=\"item-actions\">
        <div class=\"btn\" colspan=\"8\">
          <tr>
              <a href=\"";
        // line 19
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("add_review");
        echo "\" class=\"btn btn-sm btn-default\">
                  <i class=\"fa fa-plus\"></i>Add a review
              </a>
          </tr>
        </div>
        <div class=\"btn btn-sm btn-primary\">
            <tr>
                <td colspan=\"4\" align=\"center\">Load Review CSV File</td>
                <form action=\"";
        // line 27
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("upload_review_file");
        echo "\" method=\"post\" enctype=\"multipart/form-data\">
                      <input type=\"file\" name=\"reviewFile\" id=\"reviewFile\">
                      <button class=\"fa fa-plus btn-default\" type=\"submit\">Upload</button>
                </form>
            </tr>
        </div>
        <div class=\"btn navbar-right\" colspan=\"8\">
              <tr>
                  <a href=\"";
        // line 35
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("evaluate_all");
        echo "\" align=\"right\" class=\"btn btn-lg btn-primary left\">
                      <i class=\"fa fa-tint\"></i>Evaluate all
                  </a>
              </tr>
        </div>
    </div>

    <br/>
    <table class=\"table table-striped\">
        <thead>
            <tr>
                <th><i class=\"fa fa-user\"></i>Stock Id</th>
                <th><i class=\"fa fa-user\"></i>Review Id</th>
                <th><i class=\"fa fa-user\"></i>Text</th>
                <th><i class=\"fa fa-user\"></i>Score</th>
                <th><i class=\"fa fa-user\"></i>Total</th>
                <th><i class=\"fa fa-cogs\"></i>Actions</th>
            </tr>
        </thead>
        <tbody>
        ";
        // line 55
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["reviews"]) ? $context["reviews"] : $this->getContext($context, "reviews")));
        $context['_iterated'] = false;
        foreach ($context['_seq'] as $context["_key"] => $context["review"]) {
            // line 56
            echo "            <tr>
                <td>";
            // line 57
            echo twig_escape_filter($this->env, $this->getAttribute($context["review"], "stockId", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 58
            echo twig_escape_filter($this->env, $this->getAttribute($context["review"], "reviewId", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 59
            echo twig_escape_filter($this->env, $this->getAttribute($context["review"], "reviewText", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 60
            echo twig_escape_filter($this->env, $this->getAttribute($context["review"], "score", array()), "html", null, true);
            echo "</td>
                <td>";
            // line 61
            echo twig_escape_filter($this->env, $this->getAttribute($context["review"], "total", array()), "html", null, true);
            echo "</td>
                <td>
                    <div class=\"item-actions\">
                        <a href=\"";
            // line 64
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("eval_review", array("id" => $this->getAttribute($context["review"], "id", array()))), "html", null, true);
            echo "\" class=\"btn btn-sm btn-primary\">
                            <i class=\"fa fa-tint\"></i> Evaluate
                        </a>
                        <a href=\"";
            // line 67
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("modify_review", array("id" => $this->getAttribute($context["review"], "id", array()))), "html", null, true);
            echo "\" class=\"btn btn-sm btn-default\">
                            <i class=\"fa fa-edit\"></i>Modify
                        </a>
                        <a href=\"";
            // line 70
            echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("delete_review", array("id" => $this->getAttribute($context["review"], "id", array()))), "html", null, true);
            echo "\" class=\"btn btn-sm btn-default\">
                            <i class=\"fa fa-edit\"></i>Delete
                        </a>
                    </div>
                </td>
            </tr>
        ";
            $context['_iterated'] = true;
        }
        if (!$context['_iterated']) {
            // line 77
            echo "            <tr>
                <td colspan=\"6\" align=\"center\">Not reviews added.</td>
           </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['review'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 81
        echo "        </tbody>
    </table>
    <br />

    <div class=\"navigation text-center\">
        ";
        // line 86
        echo $this->env->getExtension('WhiteOctober\PagerfantaBundle\Twig\PagerfantaExtension')->renderPagerfanta((isset($context["reviews"]) ? $context["reviews"] : $this->getContext($context, "reviews")), "twitter_bootstrap3", array("routeName" => "homepage_paginated"));
        echo "
    </div>

";
        
        $__internal_28da129d751571ec7796513061289bbb4d456b1f83841d2dcbc21382663fc83a->leave($__internal_28da129d751571ec7796513061289bbb4d456b1f83841d2dcbc21382663fc83a_prof);

    }

    public function getTemplateName()
    {
        return "reviews/review.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  186 => 86,  179 => 81,  170 => 77,  158 => 70,  152 => 67,  146 => 64,  140 => 61,  136 => 60,  132 => 59,  128 => 58,  124 => 57,  121 => 56,  116 => 55,  93 => 35,  82 => 27,  71 => 19,  59 => 10,  53 => 6,  47 => 5,  35 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body_id 'review' %}

{% block main %}
    <h1>Reviews</h1>
    <div class=\"item-actions\">
        <div class=\"btn\">
          <tr>
              <a href=\"{{ path('criteria') }}\" class=\"btn-link\">
                  <i class=\"fa fa-plus\"></i>Check Criteria
              </a>
          </tr>
        </div>
    </div>
    <div class=\"item-actions\">
        <div class=\"btn\" colspan=\"8\">
          <tr>
              <a href=\"{{ path('add_review') }}\" class=\"btn btn-sm btn-default\">
                  <i class=\"fa fa-plus\"></i>Add a review
              </a>
          </tr>
        </div>
        <div class=\"btn btn-sm btn-primary\">
            <tr>
                <td colspan=\"4\" align=\"center\">Load Review CSV File</td>
                <form action=\"{{ path('upload_review_file') }}\" method=\"post\" enctype=\"multipart/form-data\">
                      <input type=\"file\" name=\"reviewFile\" id=\"reviewFile\">
                      <button class=\"fa fa-plus btn-default\" type=\"submit\">Upload</button>
                </form>
            </tr>
        </div>
        <div class=\"btn navbar-right\" colspan=\"8\">
              <tr>
                  <a href=\"{{ path('evaluate_all') }}\" align=\"right\" class=\"btn btn-lg btn-primary left\">
                      <i class=\"fa fa-tint\"></i>Evaluate all
                  </a>
              </tr>
        </div>
    </div>

    <br/>
    <table class=\"table table-striped\">
        <thead>
            <tr>
                <th><i class=\"fa fa-user\"></i>Stock Id</th>
                <th><i class=\"fa fa-user\"></i>Review Id</th>
                <th><i class=\"fa fa-user\"></i>Text</th>
                <th><i class=\"fa fa-user\"></i>Score</th>
                <th><i class=\"fa fa-user\"></i>Total</th>
                <th><i class=\"fa fa-cogs\"></i>Actions</th>
            </tr>
        </thead>
        <tbody>
        {% for review in reviews %}
            <tr>
                <td>{{ review.stockId }}</td>
                <td>{{ review.reviewId }}</td>
                <td>{{ review.reviewText }}</td>
                <td>{{ review.score }}</td>
                <td>{{ review.total }}</td>
                <td>
                    <div class=\"item-actions\">
                        <a href=\"{{ path('eval_review', { id: review.id }) }}\" class=\"btn btn-sm btn-primary\">
                            <i class=\"fa fa-tint\"></i> Evaluate
                        </a>
                        <a href=\"{{ path('modify_review', { id: review.id }) }}\" class=\"btn btn-sm btn-default\">
                            <i class=\"fa fa-edit\"></i>Modify
                        </a>
                        <a href=\"{{ path('delete_review', { id: review.id }) }}\" class=\"btn btn-sm btn-default\">
                            <i class=\"fa fa-edit\"></i>Delete
                        </a>
                    </div>
                </td>
            </tr>
        {% else %}
            <tr>
                <td colspan=\"6\" align=\"center\">Not reviews added.</td>
           </tr>
        {% endfor %}
        </tbody>
    </table>
    <br />

    <div class=\"navigation text-center\">
        {{ pagerfanta(reviews, 'twitter_bootstrap3', { routeName: 'homepage_paginated' }) }}
    </div>

{% endblock %}
", "reviews/review.html.twig", "/mnt/499689A1280E35AE/Feina/Base7Booking Application/base7/app/Resources/views/reviews/review.html.twig");
    }
}
