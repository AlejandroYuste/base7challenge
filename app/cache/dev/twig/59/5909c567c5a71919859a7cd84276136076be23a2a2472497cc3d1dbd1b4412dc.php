<?php

/* base.html.twig */
class __TwigTemplate_f0f04e9132c54aefa5c8ae2b6a68efd84337b432c6f131d8982cf715ee6dd18c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body_id' => array($this, 'block_body_id'),
            'header' => array($this, 'block_header'),
            'header_navigation_links' => array($this, 'block_header_navigation_links'),
            'body' => array($this, 'block_body'),
            'main' => array($this, 'block_main'),
            'footer' => array($this, 'block_footer'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_25586900e27c603aa7987a547364b7729d6ddb0b6a1b895d326e18833267d214 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_25586900e27c603aa7987a547364b7729d6ddb0b6a1b895d326e18833267d214->enter($__internal_25586900e27c603aa7987a547364b7729d6ddb0b6a1b895d326e18833267d214_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>

        ";
        // line 7
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 19
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>

    <body id=\"";
        // line 22
        $this->displayBlock('body_id', $context, $blocks);
        echo "\">
        ";
        // line 23
        $this->displayBlock('header', $context, $blocks);
        // line 56
        echo "
        <div class=\"container body-container\">
            ";
        // line 58
        $this->displayBlock('body', $context, $blocks);
        // line 66
        echo "        </div>

        ";
        // line 68
        $this->displayBlock('footer', $context, $blocks);
        // line 87
        echo "
        ";
        // line 88
        $this->displayBlock('javascripts', $context, $blocks);
        // line 101
        echo "    </body>
</html>
";
        
        $__internal_25586900e27c603aa7987a547364b7729d6ddb0b6a1b895d326e18833267d214->leave($__internal_25586900e27c603aa7987a547364b7729d6ddb0b6a1b895d326e18833267d214_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_09ce1191ebd4732c2b50fc3917e938aac114ff6f9dafcbd5bab47034c1b53330 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_09ce1191ebd4732c2b50fc3917e938aac114ff6f9dafcbd5bab47034c1b53330->enter($__internal_09ce1191ebd4732c2b50fc3917e938aac114ff6f9dafcbd5bab47034c1b53330_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Base7 Backend Challenge.";
        
        $__internal_09ce1191ebd4732c2b50fc3917e938aac114ff6f9dafcbd5bab47034c1b53330->leave($__internal_09ce1191ebd4732c2b50fc3917e938aac114ff6f9dafcbd5bab47034c1b53330_prof);

    }

    // line 7
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_41536822ab88e297c10c607962a3042ece3a0fdaa201fc33eca1b79262b68a22 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_41536822ab88e297c10c607962a3042ece3a0fdaa201fc33eca1b79262b68a22->enter($__internal_41536822ab88e297c10c607962a3042ece3a0fdaa201fc33eca1b79262b68a22_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 8
        echo "            ";
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "17e7ee5_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_17e7ee5_0") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("_controller/css/app_bootstrap-flatly_1.css");
            // line 14
            echo "                <link rel=\"stylesheet\" Shref=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
            ";
            // asset "17e7ee5_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_17e7ee5_1") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("_controller/css/app_font-awesome_2.css");
            echo "                <link rel=\"stylesheet\" Shref=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
            ";
            // asset "17e7ee5_2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_17e7ee5_2") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("_controller/css/app_part_3_bootstrap-datetimepicker.min_1.css");
            echo "                <link rel=\"stylesheet\" Shref=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
            ";
            // asset "17e7ee5_3"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_17e7ee5_3") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("_controller/css/app_part_3_font-lato_2.css");
            echo "                <link rel=\"stylesheet\" Shref=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
            ";
            // asset "17e7ee5_4"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_17e7ee5_4") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("_controller/css/app_part_3_highlight-solarized-light_3.css");
            echo "                <link rel=\"stylesheet\" Shref=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
            ";
            // asset "17e7ee5_5"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_17e7ee5_5") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("_controller/css/app_main_4.css");
            echo "                <link rel=\"stylesheet\" Shref=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
            ";
        } else {
            // asset "17e7ee5"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_17e7ee5") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("_controller/css/app.css");
            echo "                <link rel=\"stylesheet\" Shref=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\" />
            ";
        }
        unset($context["asset_url"]);
        // line 16
        echo "
             <link rel=\"stylesheet\" href=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("css/app.css"), "html", null, true);
        echo "\">
        ";
        
        $__internal_41536822ab88e297c10c607962a3042ece3a0fdaa201fc33eca1b79262b68a22->leave($__internal_41536822ab88e297c10c607962a3042ece3a0fdaa201fc33eca1b79262b68a22_prof);

    }

    // line 22
    public function block_body_id($context, array $blocks = array())
    {
        $__internal_7e4e51bbb567b8c4ef7143a33134d391f9f98be19fce332a4c51115d44ad14be = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7e4e51bbb567b8c4ef7143a33134d391f9f98be19fce332a4c51115d44ad14be->enter($__internal_7e4e51bbb567b8c4ef7143a33134d391f9f98be19fce332a4c51115d44ad14be_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        
        $__internal_7e4e51bbb567b8c4ef7143a33134d391f9f98be19fce332a4c51115d44ad14be->leave($__internal_7e4e51bbb567b8c4ef7143a33134d391f9f98be19fce332a4c51115d44ad14be_prof);

    }

    // line 23
    public function block_header($context, array $blocks = array())
    {
        $__internal_73bc81502e503884f81b05a878f73c9294bbb78b095741120bbd6df72ab69aea = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_73bc81502e503884f81b05a878f73c9294bbb78b095741120bbd6df72ab69aea->enter($__internal_73bc81502e503884f81b05a878f73c9294bbb78b095741120bbd6df72ab69aea_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header"));

        // line 24
        echo "            <header>
                <div class=\"navbar navbar-default navbar-static-top\" role=\"navigation\">
                    <div class=\"container\">
                        <div class=\"navbar-header\">
                            <a class=\"navbar-brand\" href=\"";
        // line 28
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("homepage");
        echo "\">
                                Base7Booking BE Challenge
                            </a>

                            <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-collapse\">
                                <span class=\"sr-only\">Toggle navigation</span>
                                <span class=\"icon-bar\"></span>
                                <span class=\"icon-bar\"></span>
                                <span class=\"icon-bar\"></span>
                            </button>
                        </div>
                        <div class=\"navbar-collapse collapse\">
                            <ul class=\"nav navbar-nav navbar-right\">

                                ";
        // line 42
        $this->displayBlock('header_navigation_links', $context, $blocks);
        // line 49
        echo "
                            </ul>
                        </div>
                    </div>
                </div>
            </header>
        ";
        
        $__internal_73bc81502e503884f81b05a878f73c9294bbb78b095741120bbd6df72ab69aea->leave($__internal_73bc81502e503884f81b05a878f73c9294bbb78b095741120bbd6df72ab69aea_prof);

    }

    // line 42
    public function block_header_navigation_links($context, array $blocks = array())
    {
        $__internal_59a8a462fc637a9052d7bb105e10b63cb8726a8bca342745eecdf86484ca998d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_59a8a462fc637a9052d7bb105e10b63cb8726a8bca342745eecdf86484ca998d->enter($__internal_59a8a462fc637a9052d7bb105e10b63cb8726a8bca342745eecdf86484ca998d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "header_navigation_links"));

        // line 43
        echo "                                    <li>
                                        <a href=\"";
        // line 44
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("homepage");
        echo "\">
                                            <i class=\"fa fa-home\" /></i> Home
                                        </a>
                                    </li>
                                ";
        
        $__internal_59a8a462fc637a9052d7bb105e10b63cb8726a8bca342745eecdf86484ca998d->leave($__internal_59a8a462fc637a9052d7bb105e10b63cb8726a8bca342745eecdf86484ca998d_prof);

    }

    // line 58
    public function block_body($context, array $blocks = array())
    {
        $__internal_6f13295b6ab3fee246ff310e67f0d0cd7b5ea8a57f602fceabfa8abbbb29f0a4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6f13295b6ab3fee246ff310e67f0d0cd7b5ea8a57f602fceabfa8abbbb29f0a4->enter($__internal_6f13295b6ab3fee246ff310e67f0d0cd7b5ea8a57f602fceabfa8abbbb29f0a4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 59
        echo "                <div class=\"row\">
                    <div id=\"main\" class=\"col-sm-12\">
                        ";
        // line 61
        $this->displayBlock('main', $context, $blocks);
        // line 63
        echo "                    </div>
                </div>
            ";
        
        $__internal_6f13295b6ab3fee246ff310e67f0d0cd7b5ea8a57f602fceabfa8abbbb29f0a4->leave($__internal_6f13295b6ab3fee246ff310e67f0d0cd7b5ea8a57f602fceabfa8abbbb29f0a4_prof);

    }

    // line 61
    public function block_main($context, array $blocks = array())
    {
        $__internal_7c1df8e07c3f672a5ce655c2b5de3d47ad1fafcea74287ebcd0cf620d0ad65cf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7c1df8e07c3f672a5ce655c2b5de3d47ad1fafcea74287ebcd0cf620d0ad65cf->enter($__internal_7c1df8e07c3f672a5ce655c2b5de3d47ad1fafcea74287ebcd0cf620d0ad65cf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main"));

        // line 62
        echo "                        ";
        
        $__internal_7c1df8e07c3f672a5ce655c2b5de3d47ad1fafcea74287ebcd0cf620d0ad65cf->leave($__internal_7c1df8e07c3f672a5ce655c2b5de3d47ad1fafcea74287ebcd0cf620d0ad65cf_prof);

    }

    // line 68
    public function block_footer($context, array $blocks = array())
    {
        $__internal_24132e14374953de3e947dad5ba1fe368e42b591c7715c1b626571203fe5ffba = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_24132e14374953de3e947dad5ba1fe368e42b591c7715c1b626571203fe5ffba->enter($__internal_24132e14374953de3e947dad5ba1fe368e42b591c7715c1b626571203fe5ffba_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "footer"));

        // line 69
        echo "            <footer>
                <div class=\"container\">
                    <div class=\"row\">
                        <div id=\"footer-copyright\" class=\"col-md-6\">
                            <p>&copy; ";
        // line 73
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, "now", "Y"), "html", null, true);
        echo " - The Symfony Project</p>
                            <p>MIT License</p>
                        </div>
                        <div id=\"footer-resources\" class=\"col-md-6\">
                            <p>
                                <a href=\"https://twitter.com/AlejandroYuste\"><i class=\"fa fa-twitter\"></i></a>
                                <a href=\"https://www.facebook.com/alejandro.yuste\"><i class=\"fa fa-facebook\"></i></a>
                                <a href=\"http://google.com\"><i class=\"fa fa-rss\"></i></a>
                            </p>
                        </div>
                    </div>
                </div>
            </footer>
        ";
        
        $__internal_24132e14374953de3e947dad5ba1fe368e42b591c7715c1b626571203fe5ffba->leave($__internal_24132e14374953de3e947dad5ba1fe368e42b591c7715c1b626571203fe5ffba_prof);

    }

    // line 88
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_7b8e013dab353c18dad01661991acf8a381558958dfa1b6317d021b4cfdfbc7c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7b8e013dab353c18dad01661991acf8a381558958dfa1b6317d021b4cfdfbc7c->enter($__internal_7b8e013dab353c18dad01661991acf8a381558958dfa1b6317d021b4cfdfbc7c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 89
        echo "            ";
        if (isset($context['assetic']['debug']) && $context['assetic']['debug']) {
            // asset "cf95333_0"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_cf95333_0") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("_controller/js/app_jquery-2.1.4_1.js");
            // line 96
            echo "                <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
            ";
            // asset "cf95333_1"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_cf95333_1") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("_controller/js/app_moment.min_2.js");
            echo "                <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
            ";
            // asset "cf95333_2"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_cf95333_2") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("_controller/js/app_bootstrap-3.3.4_3.js");
            echo "                <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
            ";
            // asset "cf95333_3"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_cf95333_3") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("_controller/js/app_highlight.pack_4.js");
            echo "                <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
            ";
            // asset "cf95333_4"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_cf95333_4") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("_controller/js/app_bootstrap-datetimepicker.min_5.js");
            echo "                <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
            ";
            // asset "cf95333_5"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_cf95333_5") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("_controller/js/app_main_6.js");
            echo "                <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
            ";
        } else {
            // asset "cf95333"
            $context["asset_url"] = isset($context['assetic']['use_controller']) && $context['assetic']['use_controller'] ? $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_assetic_cf95333") : $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("_controller/js/app.js");
            echo "                <script src=\"";
            echo twig_escape_filter($this->env, (isset($context["asset_url"]) ? $context["asset_url"] : $this->getContext($context, "asset_url")), "html", null, true);
            echo "\"></script>
            ";
        }
        unset($context["asset_url"]);
        // line 98
        echo "
            <script src=\"";
        // line 99
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("js/app.js"), "html", null, true);
        echo "\"></script>
        ";
        
        $__internal_7b8e013dab353c18dad01661991acf8a381558958dfa1b6317d021b4cfdfbc7c->leave($__internal_7b8e013dab353c18dad01661991acf8a381558958dfa1b6317d021b4cfdfbc7c_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  363 => 99,  360 => 98,  316 => 96,  311 => 89,  305 => 88,  284 => 73,  278 => 69,  272 => 68,  265 => 62,  259 => 61,  250 => 63,  248 => 61,  244 => 59,  238 => 58,  226 => 44,  223 => 43,  217 => 42,  204 => 49,  202 => 42,  185 => 28,  179 => 24,  173 => 23,  162 => 22,  153 => 17,  150 => 16,  106 => 14,  101 => 8,  95 => 7,  83 => 5,  74 => 101,  72 => 88,  69 => 87,  67 => 68,  63 => 66,  61 => 58,  57 => 56,  55 => 23,  51 => 22,  44 => 19,  42 => 7,  37 => 5,  31 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>{% block title %}Base7 Backend Challenge.{% endblock %}</title>

        {% block stylesheets %}
            {% stylesheets filter=\"scssphp\" output=\"css/app.css\"
                \"%kernel.root_dir%/Resources/assets/scss/bootstrap-flatly.scss\"
                \"%kernel.root_dir%/Resources/assets/scss/font-awesome.scss\"
                \"%kernel.root_dir%/Resources/assets/css/*.css\"
                \"%kernel.root_dir%/Resources/assets/scss/main.scss\"
            %}
                <link rel=\"stylesheet\" Shref=\"{{ asset_url }}\" />
            {% endstylesheets %}

             <link rel=\"stylesheet\" href=\"{{ asset('css/app.css') }}\">
        {% endblock %}
        <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.ico') }}\" />
    </head>

    <body id=\"{% block body_id %}{% endblock %}\">
        {% block header %}
            <header>
                <div class=\"navbar navbar-default navbar-static-top\" role=\"navigation\">
                    <div class=\"container\">
                        <div class=\"navbar-header\">
                            <a class=\"navbar-brand\" href=\"{{ path('homepage') }}\">
                                Base7Booking BE Challenge
                            </a>

                            <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-collapse\">
                                <span class=\"sr-only\">Toggle navigation</span>
                                <span class=\"icon-bar\"></span>
                                <span class=\"icon-bar\"></span>
                                <span class=\"icon-bar\"></span>
                            </button>
                        </div>
                        <div class=\"navbar-collapse collapse\">
                            <ul class=\"nav navbar-nav navbar-right\">

                                {% block header_navigation_links %}
                                    <li>
                                        <a href=\"{{ path('homepage') }}\">
                                            <i class=\"fa fa-home\" /></i> Home
                                        </a>
                                    </li>
                                {% endblock %}

                            </ul>
                        </div>
                    </div>
                </div>
            </header>
        {% endblock %}

        <div class=\"container body-container\">
            {% block body %}
                <div class=\"row\">
                    <div id=\"main\" class=\"col-sm-12\">
                        {% block main %}
                        {% endblock %}
                    </div>
                </div>
            {% endblock %}
        </div>

        {% block footer %}
            <footer>
                <div class=\"container\">
                    <div class=\"row\">
                        <div id=\"footer-copyright\" class=\"col-md-6\">
                            <p>&copy; {{ 'now'|date('Y') }} - The Symfony Project</p>
                            <p>MIT License</p>
                        </div>
                        <div id=\"footer-resources\" class=\"col-md-6\">
                            <p>
                                <a href=\"https://twitter.com/AlejandroYuste\"><i class=\"fa fa-twitter\"></i></a>
                                <a href=\"https://www.facebook.com/alejandro.yuste\"><i class=\"fa fa-facebook\"></i></a>
                                <a href=\"http://google.com\"><i class=\"fa fa-rss\"></i></a>
                            </p>
                        </div>
                    </div>
                </div>
            </footer>
        {% endblock %}

        {% block javascripts %}
            {% javascripts filter=\"?jsqueeze\" output=\"js/app.js\"
                \"%kernel.root_dir%/Resources/assets/js/jquery-2.1.4.js\"
                \"%kernel.root_dir%/Resources/assets/js/moment.min.js\"
                \"%kernel.root_dir%/Resources/assets/js/bootstrap-3.3.4.js\"
                \"%kernel.root_dir%/Resources/assets/js/highlight.pack.js\"
                \"%kernel.root_dir%/Resources/assets/js/bootstrap-datetimepicker.min.js\"
                \"%kernel.root_dir%/Resources/assets/js/main.js\" %}
                <script src=\"{{ asset_url }}\"></script>
            {% endjavascripts %}

            <script src=\"{{ asset('js/app.js') }}\"></script>
        {% endblock %}
    </body>
</html>
", "base.html.twig", "/mnt/499689A1280E35AE/Feina/Base7Booking Application/base7/app/Resources/views/base.html.twig");
    }
}
