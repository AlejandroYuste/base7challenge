<?php

/* reviews/add_review.html.twig */
class __TwigTemplate_2246cff47f98c11962615aed115b5308b50eb7ba2011a6c56b7a9f5a31b421ce extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "reviews/add_review.html.twig", 1);
        $this->blocks = array(
            'body_id' => array($this, 'block_body_id'),
            'main' => array($this, 'block_main'),
            'sidebar' => array($this, 'block_sidebar'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_355dcaba7e45eb636c2b2188f0f35c8a68b226e28c430a4ee76f17f6b76523a8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_355dcaba7e45eb636c2b2188f0f35c8a68b226e28c430a4ee76f17f6b76523a8->enter($__internal_355dcaba7e45eb636c2b2188f0f35c8a68b226e28c430a4ee76f17f6b76523a8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "reviews/add_review.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_355dcaba7e45eb636c2b2188f0f35c8a68b226e28c430a4ee76f17f6b76523a8->leave($__internal_355dcaba7e45eb636c2b2188f0f35c8a68b226e28c430a4ee76f17f6b76523a8_prof);

    }

    // line 3
    public function block_body_id($context, array $blocks = array())
    {
        $__internal_8d1fd382a2f7d71574bfe4f20fdbb3a4e21d279331c746910be98507cdc6f063 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8d1fd382a2f7d71574bfe4f20fdbb3a4e21d279331c746910be98507cdc6f063->enter($__internal_8d1fd382a2f7d71574bfe4f20fdbb3a4e21d279331c746910be98507cdc6f063_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body_id"));

        echo "add_review";
        
        $__internal_8d1fd382a2f7d71574bfe4f20fdbb3a4e21d279331c746910be98507cdc6f063->leave($__internal_8d1fd382a2f7d71574bfe4f20fdbb3a4e21d279331c746910be98507cdc6f063_prof);

    }

    // line 5
    public function block_main($context, array $blocks = array())
    {
        $__internal_b9b7402ebff155cebf1748ffb1072ac76acfb04a62b413bb45529f18b59d4053 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b9b7402ebff155cebf1748ffb1072ac76acfb04a62b413bb45529f18b59d4053->enter($__internal_b9b7402ebff155cebf1748ffb1072ac76acfb04a62b413bb45529f18b59d4053_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "main"));

        // line 6
        echo "    <h1>Add a Review</h1>

        ";
        // line 8
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start');
        echo "
        ";
        // line 9
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "stockId", array()), 'row');
        echo "
        ";
        // line 10
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "reviewText", array()), 'row');
        echo "

        <input type=\"submit\" value=\"Save\" class=\"btn btn-primary\" />

        ";
        // line 14
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock($this->getAttribute((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), "saveAndCreateNew", array()), 'widget', array("label" => "Save and add a new review", "attr" => array("class" => "btn btn-default")));
        echo "

        <a href=\"";
        // line 16
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("homepage");
        echo "\" class=\"btn btn-link\">
            Back to check reviews
        </a>
    ";
        // line 19
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "
";
        
        $__internal_b9b7402ebff155cebf1748ffb1072ac76acfb04a62b413bb45529f18b59d4053->leave($__internal_b9b7402ebff155cebf1748ffb1072ac76acfb04a62b413bb45529f18b59d4053_prof);

    }

    // line 22
    public function block_sidebar($context, array $blocks = array())
    {
        $__internal_7100dc5fe89a72d6ec8028875a91820e851ddd362ad7a7a570efdfd9a4ce5ed3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7100dc5fe89a72d6ec8028875a91820e851ddd362ad7a7a570efdfd9a4ce5ed3->enter($__internal_7100dc5fe89a72d6ec8028875a91820e851ddd362ad7a7a570efdfd9a4ce5ed3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "sidebar"));

        // line 23
        echo "    ";
        $this->displayParentBlock("sidebar", $context, $blocks);
        echo "
";
        
        $__internal_7100dc5fe89a72d6ec8028875a91820e851ddd362ad7a7a570efdfd9a4ce5ed3->leave($__internal_7100dc5fe89a72d6ec8028875a91820e851ddd362ad7a7a570efdfd9a4ce5ed3_prof);

    }

    public function getTemplateName()
    {
        return "reviews/add_review.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  99 => 23,  93 => 22,  84 => 19,  78 => 16,  73 => 14,  66 => 10,  62 => 9,  58 => 8,  54 => 6,  48 => 5,  36 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body_id 'add_review' %}

{% block main %}
    <h1>Add a Review</h1>

        {{ form_start(form) }}
        {{ form_row(form.stockId) }}
        {{ form_row(form.reviewText) }}

        <input type=\"submit\" value=\"Save\" class=\"btn btn-primary\" />

        {{ form_widget(form.saveAndCreateNew, { label: 'Save and add a new review', attr: { class: 'btn btn-default' } }) }}

        <a href=\"{{ path('homepage') }}\" class=\"btn btn-link\">
            Back to check reviews
        </a>
    {{ form_end(form) }}
{% endblock %}

{% block sidebar %}
    {{ parent() }}
{% endblock %}
", "reviews/add_review.html.twig", "/mnt/499689A1280E35AE/Feina/Base7Booking Application/base7/app/Resources/views/reviews/add_review.html.twig");
    }
}
