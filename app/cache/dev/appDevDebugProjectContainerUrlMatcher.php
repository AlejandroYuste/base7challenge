<?php

use Symfony\Component\Routing\Exception\MethodNotAllowedException;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * appDevDebugProjectContainerUrlMatcher.
 *
 * This class has been auto-generated
 * by the Symfony Routing Component.
 */
class appDevDebugProjectContainerUrlMatcher extends Symfony\Bundle\FrameworkBundle\Routing\RedirectableUrlMatcher
{
    /**
     * Constructor.
     */
    public function __construct(RequestContext $context)
    {
        $this->context = $context;
    }

    public function match($pathinfo)
    {
        $allow = array();
        $pathinfo = rawurldecode($pathinfo);
        $context = $this->context;
        $request = $this->request;

        if (0 === strpos($pathinfo, '/css/app')) {
            // _assetic_17e7ee5
            if ($pathinfo === '/css/app.css') {
                return array (  '_controller' => 'assetic.controller:render',  'name' => '17e7ee5',  'pos' => NULL,  '_format' => 'css',  '_route' => '_assetic_17e7ee5',);
            }

            if (0 === strpos($pathinfo, '/css/app_')) {
                // _assetic_17e7ee5_0
                if ($pathinfo === '/css/app_bootstrap-flatly_1.css') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => '17e7ee5',  'pos' => 0,  '_format' => 'css',  '_route' => '_assetic_17e7ee5_0',);
                }

                // _assetic_17e7ee5_1
                if ($pathinfo === '/css/app_font-awesome_2.css') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => '17e7ee5',  'pos' => 1,  '_format' => 'css',  '_route' => '_assetic_17e7ee5_1',);
                }

                if (0 === strpos($pathinfo, '/css/app_part_3_')) {
                    // _assetic_17e7ee5_2
                    if ($pathinfo === '/css/app_part_3_bootstrap-datetimepicker.min_1.css') {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => '17e7ee5',  'pos' => 2,  '_format' => 'css',  '_route' => '_assetic_17e7ee5_2',);
                    }

                    // _assetic_17e7ee5_3
                    if ($pathinfo === '/css/app_part_3_font-lato_2.css') {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => '17e7ee5',  'pos' => 3,  '_format' => 'css',  '_route' => '_assetic_17e7ee5_3',);
                    }

                    // _assetic_17e7ee5_4
                    if ($pathinfo === '/css/app_part_3_highlight-solarized-light_3.css') {
                        return array (  '_controller' => 'assetic.controller:render',  'name' => '17e7ee5',  'pos' => 4,  '_format' => 'css',  '_route' => '_assetic_17e7ee5_4',);
                    }

                }

                // _assetic_17e7ee5_5
                if ($pathinfo === '/css/app_main_4.css') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => '17e7ee5',  'pos' => 5,  '_format' => 'css',  '_route' => '_assetic_17e7ee5_5',);
                }

            }

        }

        if (0 === strpos($pathinfo, '/js/app')) {
            // _assetic_cf95333
            if ($pathinfo === '/js/app.js') {
                return array (  '_controller' => 'assetic.controller:render',  'name' => 'cf95333',  'pos' => NULL,  '_format' => 'js',  '_route' => '_assetic_cf95333',);
            }

            if (0 === strpos($pathinfo, '/js/app_')) {
                // _assetic_cf95333_0
                if ($pathinfo === '/js/app_jquery-2.1.4_1.js') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => 'cf95333',  'pos' => 0,  '_format' => 'js',  '_route' => '_assetic_cf95333_0',);
                }

                // _assetic_cf95333_1
                if ($pathinfo === '/js/app_moment.min_2.js') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => 'cf95333',  'pos' => 1,  '_format' => 'js',  '_route' => '_assetic_cf95333_1',);
                }

                // _assetic_cf95333_2
                if ($pathinfo === '/js/app_bootstrap-3.3.4_3.js') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => 'cf95333',  'pos' => 2,  '_format' => 'js',  '_route' => '_assetic_cf95333_2',);
                }

                // _assetic_cf95333_3
                if ($pathinfo === '/js/app_highlight.pack_4.js') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => 'cf95333',  'pos' => 3,  '_format' => 'js',  '_route' => '_assetic_cf95333_3',);
                }

                // _assetic_cf95333_4
                if ($pathinfo === '/js/app_bootstrap-datetimepicker.min_5.js') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => 'cf95333',  'pos' => 4,  '_format' => 'js',  '_route' => '_assetic_cf95333_4',);
                }

                // _assetic_cf95333_5
                if ($pathinfo === '/js/app_main_6.js') {
                    return array (  '_controller' => 'assetic.controller:render',  'name' => 'cf95333',  'pos' => 5,  '_format' => 'js',  '_route' => '_assetic_cf95333_5',);
                }

            }

        }

        if (0 === strpos($pathinfo, '/_')) {
            // _wdt
            if (0 === strpos($pathinfo, '/_wdt') && preg_match('#^/_wdt/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_wdt')), array (  '_controller' => 'web_profiler.controller.profiler:toolbarAction',));
            }

            if (0 === strpos($pathinfo, '/_profiler')) {
                // _profiler_home
                if (rtrim($pathinfo, '/') === '/_profiler') {
                    if (substr($pathinfo, -1) !== '/') {
                        return $this->redirect($pathinfo.'/', '_profiler_home');
                    }

                    return array (  '_controller' => 'web_profiler.controller.profiler:homeAction',  '_route' => '_profiler_home',);
                }

                if (0 === strpos($pathinfo, '/_profiler/search')) {
                    // _profiler_search
                    if ($pathinfo === '/_profiler/search') {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchAction',  '_route' => '_profiler_search',);
                    }

                    // _profiler_search_bar
                    if ($pathinfo === '/_profiler/search_bar') {
                        return array (  '_controller' => 'web_profiler.controller.profiler:searchBarAction',  '_route' => '_profiler_search_bar',);
                    }

                }

                // _profiler_purge
                if ($pathinfo === '/_profiler/purge') {
                    return array (  '_controller' => 'web_profiler.controller.profiler:purgeAction',  '_route' => '_profiler_purge',);
                }

                // _profiler_info
                if (0 === strpos($pathinfo, '/_profiler/info') && preg_match('#^/_profiler/info/(?P<about>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_info')), array (  '_controller' => 'web_profiler.controller.profiler:infoAction',));
                }

                // _profiler_phpinfo
                if ($pathinfo === '/_profiler/phpinfo') {
                    return array (  '_controller' => 'web_profiler.controller.profiler:phpinfoAction',  '_route' => '_profiler_phpinfo',);
                }

                // _profiler_search_results
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/search/results$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_search_results')), array (  '_controller' => 'web_profiler.controller.profiler:searchResultsAction',));
                }

                // _profiler
                if (preg_match('#^/_profiler/(?P<token>[^/]++)$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler')), array (  '_controller' => 'web_profiler.controller.profiler:panelAction',));
                }

                // _profiler_router
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/router$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_router')), array (  '_controller' => 'web_profiler.controller.router:panelAction',));
                }

                // _profiler_exception
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception')), array (  '_controller' => 'web_profiler.controller.exception:showAction',));
                }

                // _profiler_exception_css
                if (preg_match('#^/_profiler/(?P<token>[^/]++)/exception\\.css$#s', $pathinfo, $matches)) {
                    return $this->mergeDefaults(array_replace($matches, array('_route' => '_profiler_exception_css')), array (  '_controller' => 'web_profiler.controller.exception:cssAction',));
                }

            }

            // _twig_error_test
            if (0 === strpos($pathinfo, '/_error') && preg_match('#^/_error/(?P<code>\\d+)(?:\\.(?P<_format>[^/]++))?$#s', $pathinfo, $matches)) {
                return $this->mergeDefaults(array_replace($matches, array('_route' => '_twig_error_test')), array (  '_controller' => 'twig.controller.preview_error:previewErrorPageAction',  '_format' => 'html',));
            }

        }

        // homepage
        if (rtrim($pathinfo, '/') === '') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'homepage');
            }

            return array (  'page' => 1,  '_controller' => 'AppBundle\\Controller\\DefaultController::showReviews',  '_route' => 'homepage',);
        }

        // homepage_paginated
        if (0 === strpos($pathinfo, '/page') && preg_match('#^/page/(?P<page>[1-9]\\d*)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'homepage_paginated')), array (  '_controller' => 'AppBundle\\Controller\\DefaultController::showReviews',));
        }

        // criteria
        if ($pathinfo === '/criteria') {
            return array (  'page' => 1,  '_controller' => 'AppBundle\\Controller\\DefaultController::showCrteiria',  '_route' => 'criteria',);
        }

        // criteria_paginated
        if (0 === strpos($pathinfo, '/page') && preg_match('#^/page/(?P<page>[1-9]\\d*)$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'criteria_paginated')), array (  '_controller' => 'AppBundle\\Controller\\DefaultController::showCrteiria',));
        }

        // evaluate_all
        if (rtrim($pathinfo, '/') === '') {
            if (substr($pathinfo, -1) !== '/') {
                return $this->redirect($pathinfo.'/', 'evaluate_all');
            }

            return array (  'page' => 1,  '_controller' => 'AppBundle\\Controller\\DefaultController::evalAll',  '_route' => 'evaluate_all',);
        }

        // eval_review
        if (preg_match('#^/(?P<id>\\d+)/eval$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'eval_review')), array (  '_controller' => 'AppBundle\\Controller\\DefaultController::evalReviewPage',));
        }

        if (0 === strpos($pathinfo, '/upload_')) {
            // upload_review_file
            if ($pathinfo === '/upload_review_file') {
                return array (  '_controller' => 'AppBundle\\Controller\\DefaultController::uploadReviewFile',  '_route' => 'upload_review_file',);
            }

            // upload_criteria_file
            if ($pathinfo === '/upload_criteria_file') {
                return array (  '_controller' => 'AppBundle\\Controller\\DefaultController::uploadCriteriaFile',  '_route' => 'upload_criteria_file',);
            }

        }

        // error_cvs
        if ($pathinfo === '/error_cvs') {
            return array (  'page' => 1,  '_controller' => 'AppBundle\\Controller\\DefaultController::erroCVS',  '_route' => 'error_cvs',);
        }

        // add_review
        if ($pathinfo === '/new_review') {
            return array (  '_controller' => 'AppBundle\\Controller\\DefaultController::newReview',  '_route' => 'add_review',);
        }

        // modify_review
        if (preg_match('#^/(?P<id>\\d+)/edit_review$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'modify_review')), array (  '_controller' => 'AppBundle\\Controller\\DefaultController::modifyReview',));
        }

        // delete_review
        if (preg_match('#^/(?P<id>\\d+)/remove_review$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'delete_review')), array (  '_controller' => 'AppBundle\\Controller\\DefaultController::deleteReview',));
        }

        // add_criteria
        if ($pathinfo === '/new_criteria') {
            return array (  '_controller' => 'AppBundle\\Controller\\DefaultController::newCrtieria',  '_route' => 'add_criteria',);
        }

        // modify_criteria
        if (preg_match('#^/(?P<id>\\d+)/edit_criteria$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'modify_criteria')), array (  '_controller' => 'AppBundle\\Controller\\DefaultController::modifyCriteria',));
        }

        // delete_criteria
        if (preg_match('#^/(?P<id>\\d+)/remove_criteria$#s', $pathinfo, $matches)) {
            return $this->mergeDefaults(array_replace($matches, array('_route' => 'delete_criteria')), array (  '_controller' => 'AppBundle\\Controller\\DefaultController::deleteCriteria',));
        }

        throw 0 < count($allow) ? new MethodNotAllowedException(array_unique($allow)) : new ResourceNotFoundException();
    }
}
